package com.example.cdpackaging.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.cdpackaging.PlantApplication;
import com.example.cdpackaging.login.LoginActivity;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;


public class SMSRetriever extends BroadcastReceiver {

    ISmsListener listener;

    public void injectListener(ISmsListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
            Bundle extras = intent.getExtras();
            Status status = (Status) extras.get(SmsRetriever.EXTRA_STATUS);

            switch (status.getStatusCode()) {
                case CommonStatusCodes.SUCCESS:
                    // Get SMS message contents
                    String message = (String) extras.get(SmsRetriever.EXTRA_SMS_MESSAGE);
                    // Extract one-time code from the message and complete verification
                    // by sending the code back to your server.

                    try {
                        listener.smsReceived(message.substring(4, 9));
                        AppSettings appSettings = PlantApplication.appInstance.getAppSettings();
                        LoginActivity inst = null;//LoginsActivity.instance();
                        if (appSettings.getTokenValue() == null || appSettings.getTokenValue().isEmpty())
                            if (inst != null)
                                inst.updateOtp(message.substring(4, 9));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
                case CommonStatusCodes.TIMEOUT:
                    //  Toast.makeText(context, "Timeout", Toast.LENGTH_SHORT).show();
                    // Waiting for SMS timed out (5 minutes)
                    // Handle the error ...
                    break;
            }
        }
    }
}
