package com.example.cdpackaging.utils;

/**
 * Created by Rahul on 07-12-20.
 */
public interface ISmsListener {
    void smsReceived(String otpNumber);
}
