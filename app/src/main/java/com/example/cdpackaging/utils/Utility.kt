package com.example.cdpackaging.utils

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.net.ConnectivityManager
import android.view.Gravity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import coil.load
import com.example.cdpackaging.PlantApplication
import com.example.cdpackaging.R
import com.example.cdpackaging.model.APIError
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.HttpException
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Rahul on 07-12-20.
 */
object Utility {

    fun hideKeyboard(view: View) {
        if (view.context != null) {
            val imm =
                view.context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun openKeyboard(view: View) {
        if (view.context != null) {
            val imm =
                view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    fun dpToPx(dp: Float): Float {
        return dp * Resources.getSystem().displayMetrics.density
    }


    fun <T> safeApi(apiCall: suspend () -> T) = flow<Result<T>> {
        try {
            emit(Result.loading())
            val result = apiCall.invoke()
            emit(Result.success(result))
        } catch (throwable: Throwable) {
            when (throwable) {
                is IOException -> emit(Result.networkError())
                is HttpException -> {
                    try {
                        val code = throwable.code()
                        var detail: String = throwable.response()?.errorBody()?.source().toString()

                        //check if detail contains 'detail' key. if yes, then show detail as error detail else show as it is error detail received.
                        detail = detail.substringAfter("[text=").substringBeforeLast("]")
                        if (detail.contains("{\"detail\":"))
                            detail = Gson().fromJson(
                                throwable.response()?.errorBody()?.charStream(),
                                APIError::class.java
                            ).detail
                        emit(Result.error(code, detail))
                    } catch (e: Exception) {
                        emit(Result.error(throwable.code(), null))
                    }
                }
                else -> {
                    emit(Result.error(null, null))
                }
            }
        }

    }.flowOn(Dispatchers.IO)

    fun getCurrentDate(): String {
        val calendar = Calendar.getInstance()
        //val simpleDateFormat = SimpleDateFormat("dd MMMM yyyy", Locale.getDefault())
        val simpleDateFormat = SimpleDateFormat("MMM d, yyyy", Locale.getDefault())
        val todayDate = simpleDateFormat.format(calendar.time)
        return todayDate
    }

    fun getCurrentTime(): String {
        val simpleDateFormat = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
        val currentTime = simpleDateFormat.format(Date())
        return currentTime
    }

    fun getServerFormatCurrentDate(): String {
        val calendar = Calendar.getInstance()
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val todayDate = simpleDateFormat.format(calendar.time)
        return todayDate
    }

    fun convertDateToServerFormat(myDate: String): String {
        try {
            //val simpleDateFormat = SimpleDateFormat("dd MMMM yyyy", Locale.getDefault())
            val simpleDateFormat = SimpleDateFormat("MMM d, yyyy", Locale.getDefault())
            val date = simpleDateFormat.parse(myDate)
            val simpleDateFormat2 = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val sDate = simpleDateFormat2.format(date!!)
            return sDate
        } catch (e: Exception){
            e.printStackTrace()
        }
        return myDate
    }

    fun convertDateToDisplayFormat(myDate: String): String {
        try {
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val date = simpleDateFormat.parse(myDate)
            //val simpleDateFormat2 = SimpleDateFormat("dd MMMM yyyy", Locale.getDefault())
            val simpleDateFormat2 = SimpleDateFormat("MMM d, yyyy", Locale.getDefault())
            val sDate = simpleDateFormat2.format(date!!)
            return sDate
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return myDate
    }

    fun getTimeStamp() = System.currentTimeMillis()

    fun getToken(): String =
        "Bearer ${PlantApplication.appInstance.getAppSettings().tokenValue}"
}

/**
 * Returns true if internet found else false and displays no internet dialog, if asked.
 */
fun Context.hasNetwork(showErrorDialog: Boolean = true): Boolean {
    val manager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val networkInfo = manager.activeNetworkInfo
    var isAvailable = false
    if (networkInfo != null && networkInfo.isConnected) {
        isAvailable = true
    }

    if (!isAvailable && showErrorDialog)
        showAlertDialog()

    return isAvailable
}

/**
 * Displays message on dismiss dialog
 */
fun Context.showDismissDialog(
    title: String = getString(R.string.no_internet),
    message: String = getString(R.string.try_again)
) {
    AlertDialog.Builder(this, R.style.DialogTheme)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(getString(R.string.ok)) { dialog, _ -> dialog.cancel() }
        .setCancelable(false)
        .show()
}

/**
 * Displays message on finish dialog
 */
fun Context.showAlertDialog(title: String = "", message: String = getString(R.string.try_again)) {
    AlertDialog.Builder(this, R.style.DialogTheme)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton("Ok") { _, _ -> (this as Activity).finish() }
        .setCancelable(false)
        .show()
}

/**
 * Displays error message on finish dialog
 */
fun Context.showAlertDialog(code: Int?, message: String?) {
    AlertDialog.Builder(this, R.style.DialogTheme)
        .setTitle("")
        .setMessage(message ?: getString(R.string.try_again))
        .setPositiveButton("Ok") { _, _ ->
            if (code == 401)
                PlantApplication.logout(this as AppCompatActivity)
            else
                (this as Activity).finish()
        }
        .setCancelable(false)
        .show()
}


/**
 * Displays Toast message or error message by default.
 */
fun Context.showToast(msg: String = getString(R.string.try_again)) {
    Toast.makeText(this, msg, Toast.LENGTH_LONG).apply {
        setGravity(Gravity.BOTTOM, 0, 200)
        show()
    }
}

/**
 * Displays Toast message or error message by default.
 */
fun Context.showToast(code: Int?, detail: String?) {
    if (code == 401 || code == 406)
        PlantApplication.logout(this as AppCompatActivity)
    else if (code == 403)
        showDismissDialog("Permission Denied", detail ?: getString(R.string.try_again))

    if (code != 403)
        showToast(if (detail.isNullOrEmpty()) "Something went wrong, try again later [$code]." else detail)
}

/**
 * Displays Toast message or error message by default.
 */
fun Context.showLoginToast(code: Int?, detail: String?) {
    showToast(if (detail.isNullOrEmpty()) "Something went wrong, try again later [$code]." else detail)
}

/**
 * Downloads image from internet and displays on ImageView
 */
fun ImageView.loadImageWithCoil(url: String) {
    val imageUrl = url.substringAfter(":8002")
    if (imageUrl.isNotEmpty()) {
        load(imageUrl) {
            placeholder(drawable)                    // image to show till thumbnail or image download complete
            error(drawable)                          // image to show after download failure
            //transformations(CircleCropTransformation())      // used to circular crop downloaded image
            crossfade(true)                             // used to download & show thumbnail of size 100x100 till the original image downloaded
            //size(250, 512)                                   // resize the downloaded image in order to proper fit into imageView
        }
    }
}