package com.example.cdpackaging.utils

sealed class Result<T> {

    class Loading<T> : Result<T>()
    data class Success<T>(val data: T) : Result<T>()
    data class Error<T>(val code: Int? = null, val detail: String? = null) : Result<T>()
    class NetworkError<T> : Result<T>()

    companion object {
        fun <T> loading() = Loading<T>()

        fun <T> success(data: T) = Success(data)

        fun <T> error(code: Int?, message: String?) = Error<T>(code, message)

        fun <T> networkError() = NetworkError<T>()
    }
}