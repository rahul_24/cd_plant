package com.example.cdpackaging.utils

import com.example.cdpackaging.PlantApplication

/**
 * Created by Rahul on 07-12-20.
 */
class AppSettings {

    var email: String?
        get() {
            val sharedPref = PlantApplication.appInstance.getSharedPreferences(USER_PROFILE, 0)
            return sharedPref.getString(EMAIL, "")
        }
        set(json) {
            val sharedPref = PlantApplication.appInstance.getSharedPreferences(USER_PROFILE, 0)
            val editor = sharedPref.edit()
            editor.putString(EMAIL, json)
            editor.apply()
        }
    var todayDate: String
        get() {
            val todayDatePref = PlantApplication.appInstance.getSharedPreferences(USER_PROFILE, 0)
            return todayDatePref.getString(TODAY_DATE, "")!!
        }
        set(value) {
            val todayDatePref = PlantApplication.appInstance.getSharedPreferences(USER_PROFILE, 0)
            val todayDatePrefEditor = todayDatePref.edit()
            todayDatePrefEditor.putString(TODAY_DATE, value)
            todayDatePrefEditor.apply()
        }

    var tokenValue: String
        get() {
            val sharedPref = PlantApplication.appInstance.getSharedPreferences(USER_PROFILE, 0)
            return sharedPref.getString(TOKEN_VALUE, "")!!
        }
        set(tokenValue) {
            val sharedPref = PlantApplication.appInstance.getSharedPreferences(USER_PROFILE, 0)
            val editor = sharedPref.edit()
            editor.putString(TOKEN_VALUE, tokenValue)
            editor.apply()
        }
    var name: String
        get() {
            val sharedPref = PlantApplication.appInstance.getSharedPreferences(USER_PROFILE, 0)
            return sharedPref.getString(NAME, "")!!
        }
        set(fullname) {
            val sharedPref = PlantApplication.appInstance.getSharedPreferences(USER_PROFILE, 0)
            val editor = sharedPref.edit()
            editor.putString(NAME, fullname)
            editor.apply()
        }
    var contactNumber: String
        get() {
            val sharedPref = PlantApplication.appInstance.getSharedPreferences(USER_PROFILE, 0)
            return sharedPref.getString(CONTACT_NUMBER, "")!!
        }
        set(contactNumber) {
            val sharedPref = PlantApplication.appInstance.getSharedPreferences(USER_PROFILE, 0)
            val editor = sharedPref.edit()
            editor.putString(CONTACT_NUMBER, contactNumber)
            editor.apply()
        }

    var isJustLoginPref: Boolean
        get() {
            val JustLoginPref = PlantApplication.appInstance.getSharedPreferences(USER_PROFILE, 0)
            return JustLoginPref.getBoolean(JUST_LOGIN_PREF, false)
        }
        set(value) {
            val JustLoginPref = PlantApplication.appInstance.getSharedPreferences(USER_PROFILE, 0)
            val JustLoginPrefEditor = JustLoginPref.edit()
            JustLoginPrefEditor.putBoolean(JUST_LOGIN_PREF, value)
            JustLoginPrefEditor.apply()
        }

    var productId: Int
        get() {
            val productIdPref = PlantApplication.appInstance.getSharedPreferences(USER_PROFILE, 0)
            return productIdPref.getInt(PRODUCT_ID, 0)
        }
        set(value) {
            val productIdPref = PlantApplication.appInstance.getSharedPreferences(USER_PROFILE, 0)
            val productIdPrefEditor = productIdPref.edit()
            productIdPrefEditor.putInt(PRODUCT_ID, value)
            productIdPrefEditor.apply()
        }

    var productName: String
        get() {
            val productPref = PlantApplication.appInstance.getSharedPreferences(USER_PROFILE, 0)
            return productPref.getString(PRODUCT_NAME, "")!!
        }
        set(value) {
            val productPref = PlantApplication.appInstance.getSharedPreferences(USER_PROFILE, 0)
            val productPrefEditor = productPref.edit()
            productPrefEditor.putString(PRODUCT_NAME, value)
            productPrefEditor.apply()
        }

    var processId: Int
        get() {
            val processIdPref = PlantApplication.appInstance.getSharedPreferences(USER_PROFILE, 0)
            return processIdPref.getInt(PROCESS_ID, 0)
        }
        set(value) {
            val processIdPref = PlantApplication.appInstance.getSharedPreferences(USER_PROFILE, 0)
            val processIdPrefEditor = processIdPref.edit()
            processIdPrefEditor.putInt(PROCESS_ID, value)
            processIdPrefEditor.apply()
        }

    var processName: String
        get() {
            val processNamePref = PlantApplication.appInstance.getSharedPreferences(USER_PROFILE, 0)
            return processNamePref.getString(PROCESS_NAME,"")!!
        }
        set(value) {
            val processNamePref = PlantApplication.appInstance.getSharedPreferences(USER_PROFILE, 0)
            val processNamePrefEditor = processNamePref.edit()
            processNamePrefEditor.putString(PROCESS_NAME, value)
            processNamePrefEditor.apply()
        }


    var prodCentreId: Int
        get() {
            val prodCentreIdPref = PlantApplication.appInstance.getSharedPreferences(USER_PROFILE, 0)
            return prodCentreIdPref.getInt(PRODUCT_CENTRE_ID, 0)
        }
        set(value) {
            val prodCentreIdPref = PlantApplication.appInstance.getSharedPreferences(USER_PROFILE, 0)
            val prodCentreIdPrefEditor = prodCentreIdPref.edit()
            prodCentreIdPrefEditor.putInt(PRODUCT_CENTRE_ID, value)
            prodCentreIdPrefEditor.apply()
        }
    fun clearSharedPreferences() {
        val userProfilePref= PlantApplication.appInstance.getSharedPreferences(USER_PROFILE, 0)
        val editor = userProfilePref.edit()
        editor.clear().apply()
    }

    companion object {
        const val USER_PROFILE = "user_profile"
        const val TOKEN_VALUE = "token_value"
        const val TODAY_DATE = "delivery_today_date"
        const val NAME = "email"
        const val EMAIL = "email"
        const val CONTACT_NUMBER = "contact_number"
        const val PROCESS_ID = "process_id"
        const val PROCESS_NAME = "process_name"
        const val PRODUCT_ID = "product_id"
        const val PRODUCT_NAME = "product_name"
        const val PRODUCT_CENTRE_ID = "prod_centre_id"
        const val JUST_LOGIN_PREF = "just_login_pref"
        const val RESET_ALL_PROCESS_PREF = "reset_all_process"
    }
}