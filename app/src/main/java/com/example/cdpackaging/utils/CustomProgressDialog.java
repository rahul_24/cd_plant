package com.example.cdpackaging.utils;

import android.app.Dialog;
import android.content.Context;

import androidx.appcompat.app.AlertDialog;

import com.example.cdpackaging.R;


public class CustomProgressDialog {

    private static Dialog dialog;

    public static void show(Context context) {
        if (dialog == null || !dialog.isShowing()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppTheme_Dialog);
            builder.setView(R.layout.layout_progress_bar);
            dialog = builder.create();
            dialog.setCancelable(false);
            dialog.show();
        }
    }

    public static void dismiss() {
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
