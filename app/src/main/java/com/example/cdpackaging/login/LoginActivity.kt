package com.example.cdpackaging.login

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.cdpackaging.HomeActivity
import com.example.cdpackaging.PlantApplication
import com.example.cdpackaging.R
import com.example.cdpackaging.model.OtpRequestModel
import com.example.cdpackaging.model.VerifyRequestModel
import com.example.cdpackaging.model.VerifyResponseModel
import com.example.cdpackaging.network.PlantApi
import com.example.cdpackaging.room.view_model.CustomViewModel
import com.example.cdpackaging.utils.*
import com.example.cdpackaging.utils.Utility.getTimeStamp
import com.example.cdpackaging.utils.Utility.hideKeyboard
import com.example.cdpackaging.utils.Utility.safeApi
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.credentials.Credential
import com.google.android.gms.auth.api.credentials.HintRequest
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.auth.api.phone.SmsRetrieverClient
import com.google.android.gms.common.api.GoogleApiClient
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.flow.collect
import org.koin.android.viewmodel.ext.android.viewModel


class LoginActivity : AppCompatActivity() {
    private val customViewModel : CustomViewModel by viewModel()
    private val RESOLVE_HINT: Int = 201
    private var id : Int? = null
    private var showingOTP: Boolean = false
    private val smsRetriever by lazy { SMSRetriever() }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        handleListener()

        btn_action.setOnClickListener { if (showingOTP) verifyOtp() else login() }
    }

    private fun handleListener() {
        setSpannable()
        handleEts()
        requestHint()
        startSmsClient()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(smsRetriever)
    }

    private fun startSmsClient() {
        val client: SmsRetrieverClient = SmsRetriever.getClient(this)
        val retriever = client.startSmsRetriever()
        retriever.addOnSuccessListener {
            val listener = ISmsListener { otpNumber -> onSmsReceived(otpNumber) }
            smsRetriever.injectListener(listener)
            registerReceiver(smsRetriever, IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION))
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RESOLVE_HINT && resultCode == Activity.RESULT_OK) {
            try {
                val credential = data!!.getParcelableExtra<Credential>(Credential.EXTRA_KEY)!!
                val number = credential.id.substring(3)
                et_phone.setText(number)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun onSmsReceived(otpNumber: String?) {
        et_pin.setText(otpNumber)
    }

    private fun requestHint() {
        val mGoogleApiClient = GoogleApiClient.Builder(this)
                .addApi(Auth.CREDENTIALS_API)
                .build()
        val hintRequest = HintRequest.Builder()
                .setPhoneNumberIdentifierSupported(true)
                .build()

        val intent = Auth.CredentialsApi.getHintPickerIntent(
                mGoogleApiClient, hintRequest
        )
        startIntentSenderForResult(
                intent.intentSender,
                RESOLVE_HINT, null, 0, 0, 0
        )
    }

    private fun handleEts() {
        et_phone.afterTextChanged {
            if (it.length >= 10) {
                btn_action.isEnabled = true
                login()
            } else {
                btn_action.isEnabled = false
            }
        }
        et_pin.afterTextChanged {
            if (it.length >= 5) {
                btn_action.isEnabled = true
                verifyOtp()
            } else {
                btn_action.isEnabled = false
            }
        }
    }

    private fun setSpannable() {
        tv_resend.makeLinks(Pair("Resend", View.OnClickListener {
            et_pin.setText("")
            login()
        }))
    }


    private fun login() {
        Log.d("ApiCall", "LoginActivity::requestOTP")

        customViewModel.launchDataLoad {
            safeApi {
                PlantApi.RetroFitService.requestOTP(OtpRequestModel(et_phone.text.toString(), getTimeStamp()))
            }.collect { response ->
                when(response){
                    is Result.Loading -> { CustomProgressDialog.show(this) }
                    is Result.Error -> {
                        CustomProgressDialog.dismiss()
                        //showToast(response.code, response.detail)
                        showDismissDialog("Error [${response.code}]", response.detail!!)
                        hideKeyboard(et_phone)
                    }
                    is Result.NetworkError -> {
                        CustomProgressDialog.dismiss()
                        showToast()
                    }
                    is Result.Success -> {
                        CustomProgressDialog.dismiss()
                        id = response.data.id
                        switchToOtp()
                    }
                }
            }
        }
    }

    private fun verifyOtp() {
        Log.d("ApiCall", "LoginActivity::verifyOTP")

        customViewModel.launchDataLoad {
            safeApi {
                PlantApi.RetroFitService.verifyOTP(VerifyRequestModel(id!!, et_pin.text.toString()))
            }.collect { response ->
                when(response){
                    is Result.Loading -> { CustomProgressDialog.show(this) }
                    is Result.Error -> {
                        et_pin.setText("")
                        CustomProgressDialog.dismiss()
                        showLoginToast(response.code, response.detail)
                    }
                    is Result.NetworkError -> {
                        CustomProgressDialog.dismiss()
                        showToast()
                    }
                    is Result.Success -> {
                        CustomProgressDialog.dismiss()
                        hideKeyboard(btn_action)
                        saveDataAndLogin(response.data)
                    }
                }
            }
        }
    }


    private fun saveDataAndLogin(model: VerifyResponseModel) {
        val appSettings = PlantApplication.appInstance.getAppSettings()
        appSettings.tokenValue = model.access_token
        appSettings.contactNumber = et_phone.text.toString()
        appSettings.isJustLoginPref = true

        val intent = Intent(this, HomeActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }

    private fun switchToOtp() {
        showingOTP = true
        et_phone.visibility = View.INVISIBLE
        divider.visibility = View.INVISIBLE
        et_pin.visibility = View.VISIBLE
        tv_desc.text = getString(R.string.enter_otp)
        tv_resend.visibility = View.VISIBLE
        btn_action.visibility = View.VISIBLE
        btn_action.isEnabled = false
        btn_action.text = getString(R.string.verify)
        et_pin.requestFocus()
        Utility.openKeyboard(et_pin)
    }

    fun updateOtp(substring: String) {
        et_pin.setText(substring)
    }

    override fun onBackPressed() {
        if (et_pin.visibility == View.VISIBLE) {
            val builder = AlertDialog.Builder(this)
            builder.setCancelable(false)
            builder.setMessage("Do you want to enter mobile number again")
            builder.setPositiveButton("Yes") { _, _ -> switchToPhone() }
            builder.setNegativeButton("No", null)
            builder.create().show()
        } else {
            val startMain = Intent(Intent.ACTION_MAIN)
            startMain.addCategory(Intent.CATEGORY_HOME)
            startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(startMain)
        }
    }

    private fun switchToPhone() {
        finish()
        startActivity(intent)
    }
}

// View Extension function
private fun TextView.makeLinks(vararg links: Pair<String, View.OnClickListener>) {
    val spannableString = SpannableString(this.text)
    for (link in links) {
        val clickableSpan = object : ClickableSpan() {
            override fun onClick(view: View) {
                Selection.setSelection((view as TextView).text as Spannable, 0)
                view.invalidate()
                link.second.onClick(view)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
            }
        }
        val startIndexOfLink = this.text.toString().indexOf(link.first)
        spannableString.setSpan(
                clickableSpan, startIndexOfLink, startIndexOfLink + link.first.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }
    this.movementMethod = LinkMovementMethod.getInstance()
    this.setText(spannableString, TextView.BufferType.SPANNABLE)
}

private fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

    })
}
