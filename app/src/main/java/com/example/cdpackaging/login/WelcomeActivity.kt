package com.example.cdpackaging.login

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.example.cdpackaging.HomeActivity
import com.example.cdpackaging.PlantApplication
import com.example.cdpackaging.R


class WelcomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        Handler().postDelayed({ sendToOtherScreen() }, 2000)
    }

    private fun sendToOtherScreen() {
        val appSettings = PlantApplication.appInstance.getAppSettings()
        if (appSettings.tokenValue.isEmpty()) {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        } else {
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}
