package com.example.cdpackaging.network

import com.example.cdpackaging.model.*
import com.example.cdpackaging.network.PlantApi.LOG_SHEET
import com.example.cdpackaging.network.PlantApi.PROCESS
import com.example.cdpackaging.network.PlantApi.PROFILE
import com.example.cdpackaging.network.PlantApi.REQUEST_OTP_URL
import com.example.cdpackaging.network.PlantApi.VERIFY_OTP_URL
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

object PlantApi {
    //Production Server
    //const val SERVER_URL = "http://delivery.countrydelight.in/api/"

    //Test Server
    //const val SERVER_URL = "https://test.countrydelight.in/api/"
    const val SERVER_URL = "http://deliveryserver-test-env.eba-jcivrutc.us-west-2.elasticbeanstalk.com/api/"

    const val REQUEST_OTP_URL = "auth/v1/login/otp/"
    const val VERIFY_OTP_URL = "auth/v1/login/verify/"
    const val PROCESS = "plant/process/?production_centre_id"
    const val SUB_PROCESS = SERVER_URL + "plant/process/"
    const val PROFILE = "plant/profile/"
    const val LOG_SHEET = "plant/log_sheet/"


    // create retrofit instance
    private val client: OkHttpClient = OkHttpClient.Builder()
        .connectTimeout(30, TimeUnit.SECONDS)
        //.writeTimeout(30, TimeUnit.SECONDS)       // write timeout
        .readTimeout(30, TimeUnit.SECONDS)  // read timeout
        .build()

    private val retrofit = Retrofit.Builder()
        .baseUrl(SERVER_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val RetroFitService: ApiService by lazy { retrofit.create(ApiService::class.java) }
}

interface ApiService {
    @POST(REQUEST_OTP_URL)
    suspend fun requestOTP(@Body params: OtpRequestModel): OtpResponseModel

    @POST(VERIFY_OTP_URL)
    suspend fun verifyOTP(@Body params: VerifyRequestModel): VerifyResponseModel

    @GET(PROFILE)
    suspend fun getProfile(@Header("Authorization") token: String): ProfileResponse

    @GET(PROCESS)
    suspend fun getProcess(
        @Header("Authorization") token: String,
        @Query("production_centres") production_centre_id: String
    ): List<ProcessResponse>

    @GET
    suspend fun getSubProcess(
        @Header("Authorization") token: String,
        @Url url: String,
    ): List<SubProcessResponse>

    @POST(LOG_SHEET)
    suspend fun sendLogSheet(
        @Header("Authorization") token: String,
        @Body logSheetRequestList: LogSheetRequest
    ): Any //LogSheetResponse

    @GET(LOG_SHEET)
    suspend fun getReports(
        @Header("Authorization") token: String,
        @Query("date") date: String? = null,
        @Query("page") pageNo: String? = null,
        @Query("date_gt") startDate: String? = null,
        @Query("date_lt") endDate: String? = null,
    ): ReportsResponse

    @GET
    suspend fun getNextReports(
        @Header("Authorization") token: String,
        @Url url: String
    ): ReportsResponse
}