package com.example.cdpackaging

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.cdpackaging.home.HomeFragment
import com.example.cdpackaging.network.PlantApi
import com.example.cdpackaging.profile.ProfileFragment
import com.example.cdpackaging.profile.SettingsFragment
import com.example.cdpackaging.reports.ReportsFragment
import com.example.cdpackaging.room.DataConverter
import com.example.cdpackaging.room.view_model.ProcessViewModel
import com.example.cdpackaging.tasks.TasksFragment
import com.example.cdpackaging.utils.*
import com.example.cdpackaging.utils.Utility.getToken
import com.example.cdpackaging.utils.Utility.safeApi
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.model.AppUpdateType.IMMEDIATE
import com.google.android.play.core.install.model.UpdateAvailability
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.flow.collect
import org.koin.android.viewmodel.ext.android.viewModel

class HomeActivity : AppCompatActivity() {
    private val processViewModel: ProcessViewModel by viewModel()
    private lateinit var fragment: Fragment
    private companion object{
        private const val REQUEST_CODE_UPDATE = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        manageNavigation()

        /*if (PlantApplication.appInstance.getAppSettings().isJustLoginPref) fetchProfile()*/
        PlantApplication.appInstance.startWorker()
        checkInAppUpdate()
    }

    private fun checkInAppUpdate(){
        val updateManager = AppUpdateManagerFactory.create(this)
        updateManager.appUpdateInfo.addOnSuccessListener {
            if (it.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE &&
                it.isUpdateTypeAllowed(IMMEDIATE)) {
                updateManager.startUpdateFlowForResult(
                    it,
                    IMMEDIATE,
                    this,
                    REQUEST_CODE_UPDATE)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        val updateManager = AppUpdateManagerFactory.create(this)
        updateManager.appUpdateInfo
            .addOnSuccessListener {
                if (it.updateAvailability() ==
                    UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                    updateManager.startUpdateFlowForResult(
                        it,
                        IMMEDIATE,
                        this,
                        REQUEST_CODE_UPDATE)
                }
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_UPDATE) {
            if (requestCode != RESULT_OK) {
                checkInAppUpdate()
            }
        }
    }


    private fun manageNavigation() {
        navigation.setOnNavigationItemSelectedListener { item ->
            container.removeAllViews()
            CustomProgressDialog.dismiss()
            when (item.itemId) {
                R.id.navigation_home -> {
                    fragment = HomeFragment()
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.container, fragment)
                        .commitAllowingStateLoss()
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.navigation_tasks -> {
                    fragment = TasksFragment()
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.container, fragment)
                        .commitAllowingStateLoss()
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.navigation_reports -> {
                    fragment = ReportsFragment()
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.container, fragment)
                        .commitAllowingStateLoss()
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.navigation_profile -> {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.container, SettingsFragment())
                        .commitAllowingStateLoss()
                    return@setOnNavigationItemSelectedListener true
                }
            }
            true
        }
        navigation.selectedItemId = R.id.navigation_home
    }

    internal fun fetchProfile() {
        Log.d("ApiCall", "HomeActivity::fetchProfile")

        processViewModel.launchDataLoad {
            safeApi {
                PlantApi.RetroFitService.getProfile(getToken())
            }.collect { response ->
                when (response) {
                    is Result.Loading -> {
                        CustomProgressDialog.show(this)
                    }
                    is Result.Error -> {
                        CustomProgressDialog.dismiss()
                        showToast(response.code, response.detail)
                    }
                    is Result.NetworkError -> {
                        CustomProgressDialog.dismiss()
                        showToast()
                    }
                    is Result.Success -> {
                        processViewModel.addUserProfile(DataConverter().getUserProfile(response.data))
                        PlantApplication.appInstance.getAppSettings().name = response.data.name ?: ""
                        PlantApplication.appInstance.getAppSettings().prodCentreId = response.data.production_centre?.id ?: -1
                        fetchProcess()
                    }
                }
            }
        }
    }

    fun fetchProcess(showLoader : Boolean = true) {
        val centreId = PlantApplication.appInstance.getAppSettings().prodCentreId
        if (centreId == -1)
            return

        Log.d("ApiCall", "HomeActivity::fetchProcess")

        processViewModel.launchDataLoad {
            safeApi {
                PlantApi.RetroFitService.getProcess(getToken(), centreId.toString())
            }.collect { response ->
                when (response) {
                    is Result.Loading -> {
                        if (showLoader)
                            CustomProgressDialog.show(this)
                    }
                    is Result.Error -> {
                        if (showLoader){
                            CustomProgressDialog.dismiss()
                            showToast(response.code, response.detail)
                        }
                    }
                    is Result.NetworkError -> {
                        if (showLoader){
                            CustomProgressDialog.dismiss()
                            showToast()
                        }
                    }
                    is Result.Success -> {
                        CustomProgressDialog.dismiss()
                        processViewModel.addAllProcess(DataConverter().getProductList(response.data))
                        PlantApplication.appInstance.getAppSettings().isJustLoginPref = false
                    }
                }
            }
        }
    }


    override fun onBackPressed() {
        if (navigation.selectedItemId == R.id.navigation_home) {
            if (!(fragment as HomeFragment).onBackPressed()) {
                finish()
            }
        } else if (navigation.selectedItemId == R.id.navigation_tasks) {
            if (!(fragment as TasksFragment).onBackPressed()) {
                navigation.selectedItemId = R.id.navigation_home
            }
        } else if (navigation.selectedItemId == R.id.navigation_reports) {
            if (!(fragment as ReportsFragment).onBackPressed()) {
                navigation.selectedItemId = R.id.navigation_home
            }
        }

        else
            navigation.selectedItemId = R.id.navigation_home
    }

    override fun onDestroy() {
        super.onDestroy()
        PlantApplication.appInstance.stopWorker()
    }
}