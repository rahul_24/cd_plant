package com.example.cdpackaging.tasks

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.cdpackaging.HomeActivity
import com.example.cdpackaging.R
import kotlinx.android.synthetic.main.fragment_reports.view.*

class TasksFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val view = inflater.inflate(R.layout.fragment_tasks, container, false)
        view.img_back.setOnClickListener {
            val activity = context as HomeActivity
            activity.onBackPressed()
        }
        return view
    }

    fun onBackPressed(): Boolean {
        return false
    }
}