package com.example.cdpackaging.profile

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.ViewModelStoreOwner
import androidx.recyclerview.widget.RecyclerView
import com.example.cdpackaging.PlantApplication
import com.example.cdpackaging.PlantApplication.Companion.logout
import com.example.cdpackaging.R
import com.example.cdpackaging.model.ProfileItem
import com.example.cdpackaging.room.view_model.ProcessViewModel
import com.example.cdpackaging.utils.hasNetwork
import com.example.cdpackaging.utils.showToast
import kotlinx.coroutines.runBlocking
import org.koin.android.viewmodel.ext.android.viewModel

class ProfileAdapter(
    private val values: List<ProfileItem>,
    private val context: Context
) : RecyclerView.Adapter<ProfileAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.cell_profile, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.imgView.setImageResource(if (item.name == "Logout") (R.drawable.ic_baseline_logout_24) else if (item.name == "Sync Data") R.drawable.ic_baseline_sync_24 else R.drawable.ic_baseline_assignment_24)
        holder.tvItem.text = item.name

        holder.root.setOnClickListener {
            val processViewModel: ProcessViewModel by (context as ViewModelStoreOwner).viewModel()
            with(context) {
                when (item.name) {
                    "Sync Data" -> {
                        if (hasNetwork(false)) {
                            PlantApplication.appInstance.restartWorker(true)
                            //showToast("Syncing data...")
                        } else showToast()
                    }
                    "Restart All Process" -> {
                        val count =
                            runBlocking { processViewModel.getInProgressOrNonSyncProcessCount() }
                        if (count == 0) {
                            processViewModel.restartAllProcess()
                            showToast("Process restarted")
                        } else {
                            val inProgressCount =
                                runBlocking { processViewModel.getInProgressProcessCount() }
                            val message =
                                if (inProgressCount > 0) getString(R.string.inprogress_error_msg) else
                                    getString(R.string.unsynced_error_msg)

                            val dialog = AlertDialog.Builder(context, R.style.DialogTheme)
                            dialog.setTitle(getString(R.string.request_denied))
                            dialog.setMessage(message)
                            dialog.setPositiveButton(getString(R.string.ok)) { _, _ -> }
                            dialog.show()
                        }
                    }
                    "Clear App Data" -> {
                        //PlantApplication.appInstance.getAppSettings().name = ""
                        //PlantApplication.appInstance.getAppSettings().contactNumber = ""
                        PlantApplication.appInstance.getAppSettings().productId = 0
                        PlantApplication.appInstance.getAppSettings().productName = ""
                        PlantApplication.appInstance.getAppSettings().processId = 0
                        PlantApplication.appInstance.getAppSettings().processName = ""
                        //PlantApplication.appInstance.stopWorker()
                        processViewModel.clearAppData()
                        showToast("Data cleared")
                    }
                    "Logout" -> {
                        val count =
                            runBlocking { processViewModel.getInProgressOrNonSyncProcessCount() }
                        if (count == 0) {
                            val dialog = AlertDialog.Builder(this, R.style.DialogTheme)
                            dialog.setTitle(getString(R.string.logout))
                            dialog.setMessage(getString(R.string.are_you_sure))
                            dialog.setPositiveButton(getString(R.string.yes)) { _, _ ->
                                logout(
                                    context as AppCompatActivity
                                )
                            }
                            dialog.setNegativeButton(getString(R.string.cancel)) { _, _ -> }
                            dialog.show()
                        } else {
                            val dialog = AlertDialog.Builder(context, R.style.DialogTheme)
                            dialog.setTitle(getString(R.string.request_denied))
                            dialog.setMessage(getString(R.string.logout_error_msg))
                            dialog.setPositiveButton(getString(R.string.ok)) { _, _ -> }
                            dialog.show()
                        }
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val root: ConstraintLayout = view.findViewById(R.id.root)
        val imgView: ImageView = view.findViewById(R.id.img_item)
        val tvItem: TextView = view.findViewById(R.id.tv_product)

        override fun toString(): String {
            return super.toString() + " '" + tvItem.text + "'"
        }
    }
}