package com.example.cdpackaging.profile

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.cdpackaging.PlantApplication
import com.example.cdpackaging.R
import com.example.cdpackaging.model.ProfileItem
import com.example.cdpackaging.network.PlantApi
import com.example.cdpackaging.room.DataConverter
import com.example.cdpackaging.room.view_model.ProcessViewModel
import com.example.cdpackaging.utils.Result
import com.example.cdpackaging.utils.Utility
import com.example.cdpackaging.utils.Utility.safeApi
import com.example.cdpackaging.utils.showToast
import kotlinx.android.synthetic.main.fragment_profile_list.view.*
import kotlinx.coroutines.flow.collect
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * A fragment representing a list of Items.Processes
 */
class ProfileFragment : Fragment() {

    val processViewModel: ProcessViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.fragment_profile_list, container, false)

        val itemList = mutableListOf(
            ProfileItem("Sync Data"),
            ProfileItem("Restart All Process"),
            //ProfileItem("Clear App Data"),
            ProfileItem("Logout")
        )

        with(view.recycler_profile) {
            adapter = ProfileAdapter(itemList, context)
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }

        val mobile = PlantApplication.appInstance.getAppSettings().contactNumber
        processViewModel.getUserProfileLiveData(mobile).observe(requireActivity(), {
            if (it == null)
                fetchProfile()
            else {
                view.tv_name.text = it.name
                view.tv_email.text = it.email
                view.tv_mobile.text = it.mobile
                view.tv_centre.text = it.centre_name
            }
        })

        return view
    }

    private fun fetchProfile() {
        Log.d("ApiCall", "ProfileFragment::getProfile")

        processViewModel.launchDataLoad {
            safeApi {
                PlantApi.RetroFitService.getProfile(Utility.getToken())
            }.collect { response ->
                if (response is Result.Error) {
                    context?.showToast(response.code, response.detail)
                } else if (response is Result.Success) {
                    processViewModel.addUserProfile(DataConverter().getUserProfile(response.data))
                    PlantApplication.appInstance.getAppSettings().prodCentreId =
                        response.data.production_centre?.id ?: -1
                }
            }
        }
    }

}