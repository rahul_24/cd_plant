package com.example.cdpackaging.profile

import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.cdpackaging.HomeActivity
import com.example.cdpackaging.PlantApplication
import com.example.cdpackaging.R
import com.example.cdpackaging.network.PlantApi
import com.example.cdpackaging.room.DataConverter
import com.example.cdpackaging.room.view_model.ProcessViewModel
import com.example.cdpackaging.utils.Result
import com.example.cdpackaging.utils.Utility
import com.example.cdpackaging.utils.hasNetwork
import com.example.cdpackaging.utils.showToast
import kotlinx.android.synthetic.main.fragment_settings.view.*
import kotlinx.android.synthetic.main.fragment_settings.view.img_back
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.runBlocking
import org.koin.android.viewmodel.ext.android.viewModel

class SettingsFragment : Fragment() {
    private lateinit var imgBack: ImageView
    private lateinit var tvName: TextView
    private lateinit var tvCenter: TextView
    private lateinit var tvMobile: TextView
    private lateinit var tvEmail: TextView
    private lateinit var tvSupport: TextView
    private lateinit var tvSyncProcess: TextView
    private lateinit var tvRestartProcess: TextView
    private lateinit var tvUserManual: TextView
    private lateinit var tvLogout: TextView
    private lateinit var tvVersion: TextView

    val processViewModel: ProcessViewModel by viewModel()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_settings, container, false)

        imgBack = view.img_back
        tvName = view.tvName
        tvCenter = view.tvCenter
        tvMobile = view.tvMobile
        tvEmail = view.tvEmail
        tvSupport = view.tvSupport
        tvSyncProcess = view.tvSyncProcess
        tvRestartProcess = view.tvRestartProcess
        tvUserManual = view.tvUserManual
        tvLogout = view.tvLogout
        tvVersion = view.tvVersion

        showProfile()
        manageActionListeners()
        return view
    }

    private fun manageActionListeners() {
        try {
            val pInfo = context!!.packageManager.getPackageInfo(context!!.packageName, 0)
            val version = pInfo.versionName
            tvVersion.text = "Version $version"
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        imgBack.setOnClickListener {
            val activity = context as HomeActivity
            activity.onBackPressed()
        }

        tvSyncProcess.setOnClickListener {
            if (context!!.hasNetwork(false)) {
                PlantApplication.appInstance.restartWorker(true)
                //showToast("Syncing data...")
            } else context?.showToast()
        }

        tvRestartProcess.setOnClickListener {
            val count = runBlocking { processViewModel.getInProgressOrNonSyncProcessCount() }
            if (count == 0) {
                processViewModel.restartAllProcess()
                context?.showToast("Process restarted")
            } else {
                val inProgressCount = runBlocking { processViewModel.getInProgressProcessCount() }
                val message = if (inProgressCount > 0) getString(R.string.inprogress_error_msg) else
                    getString(R.string.unsynced_error_msg)

                val dialog = AlertDialog.Builder(context!!, R.style.DialogTheme)
                dialog.setTitle(getString(R.string.request_denied))
                dialog.setMessage(message)
                dialog.setPositiveButton(getString(R.string.ok)) { _, _ -> }
                dialog.show()
            }
        }

        tvUserManual.setOnClickListener {
            //PlantApplication.appInstance.getAppSettings().name = ""
            //PlantApplication.appInstance.getAppSettings().contactNumber = ""
            PlantApplication.appInstance.getAppSettings().productId = 0
            PlantApplication.appInstance.getAppSettings().productName = ""
            PlantApplication.appInstance.getAppSettings().processId = 0
            PlantApplication.appInstance.getAppSettings().processName = ""
            //PlantApplication.appInstance.stopWorker()
            processViewModel.clearAppData()
            context?.showToast("Data cleared")
        }

        tvLogout.setOnClickListener {
            val count = runBlocking { processViewModel.getInProgressOrNonSyncProcessCount() }
            if (count == 0) {
                val dialog = AlertDialog.Builder(context!!, R.style.DialogTheme)
                dialog.setTitle(getString(R.string.logout))
                dialog.setMessage(getString(R.string.are_you_sure))
                dialog.setPositiveButton(getString(R.string.yes)) { _, _ ->
                    PlantApplication.logout(
                        context as AppCompatActivity
                    )
                }
                dialog.setNegativeButton(getString(R.string.cancel)) { _, _ -> }
                dialog.show()
            } else {
                val inProgressCount = runBlocking { processViewModel.getInProgressProcessCount() }
                val message = if (inProgressCount > 0) getString(R.string.inprogress_error_msg) else
                    getString(R.string.unsynced_error_msg)

                val dialog = AlertDialog.Builder(context!!, R.style.DialogTheme)
                dialog.setTitle(getString(R.string.request_denied))
                dialog.setMessage(message)
                dialog.setPositiveButton(getString(R.string.ok)) { _, _ -> }
                dialog.show()
            }
        }
    }

    private fun showProfile() {
        val mobile = PlantApplication.appInstance.getAppSettings().contactNumber
        processViewModel.getUserProfileLiveData(mobile).observe(requireActivity(), {
            if (it == null)
                fetchProfile()
            else {
                tvName.text = it.name
                tvMobile.text = it.mobile
                tvEmail.text = it.email
                tvCenter.text = it.centre_name
            }
        })
    }

    private fun fetchProfile() {
        Log.d("ApiCall", "ProfileFragment::getProfile")

        processViewModel.launchDataLoad {
            Utility.safeApi {
                PlantApi.RetroFitService.getProfile(Utility.getToken())
            }.collect { response ->
                if (response is Result.Error) {
                    context?.showToast(response.code, response.detail)
                } else if (response is Result.Success) {
                    processViewModel.addUserProfile(DataConverter().getUserProfile(response.data))
                    PlantApplication.appInstance.getAppSettings().prodCentreId =
                        response.data.production_centre?.id ?: -1
                }
            }
        }
    }
}