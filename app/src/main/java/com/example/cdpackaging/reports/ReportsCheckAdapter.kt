package com.example.cdpackaging.reports

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.cdpackaging.R
import com.example.cdpackaging.home.CheckListAdapter
import com.example.cdpackaging.room.relation.ReportsSubProcessWithCheckList
import com.example.cdpackaging.utils.Utility.convertDateToDisplayFormat
import kotlinx.android.synthetic.main.layout_date_picker.view.et_date
import kotlinx.android.synthetic.main.layout_date_picker.view.tv_title
import kotlinx.android.synthetic.main.layout_date_time_picker.view.*
import kotlinx.android.synthetic.main.layout_edittext.view.*
import kotlinx.android.synthetic.main.layout_quantity_selecter.view.*
import kotlinx.android.synthetic.main.layout_report_date_picker.view.*
import kotlinx.android.synthetic.main.layout_spinner.view.*
import kotlinx.android.synthetic.main.layout_switch.view.*
import kotlinx.android.synthetic.main.layout_time_picker.view.*
import kotlinx.android.synthetic.main.layout_time_picker.view.tv_title_start_time
import java.util.*


class ReportsCheckAdapter(
    private val fragment: ReportsFragment,
    private val subProcessList: List<ReportsSubProcessWithCheckList>,
    private var sequence: Int,
) : RecyclerView.Adapter<ReportsCheckAdapter.MyViewHolder>() {

    private val activity = fragment.activity!!

    companion object {
        private const val VIEW_TYPE0 = 0
        private const val VIEW_TYPE1 = 1
        private const val VIEW_TYPE2 = 2
        private const val VIEW_TYPE3 = 3
        private const val VIEW_TYPE4 = 4
        private const val VIEW_TYPE5 = 5
        private const val VIEW_TYPE6 = 6
        private const val VIEW_TYPE7 = 7
        private const val VIEW_TYPE8 = 8
    }

    override fun getItemViewType(position: Int): Int {
        val checkList = subProcessList[sequence].checkLists[position]

        //As we don't create any view for End Time SubTitle, so we return empty view.
        if (checkList.field_type.equals("TIME", ignoreCase = true) && previousCheckListHasSameNameAndType(position))
            return VIEW_TYPE8

        if (checkList.getAvailableChoices().isNotEmpty())
            return VIEW_TYPE7

        return when (checkList.field_type) {
            "REPORT DATE" -> VIEW_TYPE0
            "DATE" -> VIEW_TYPE1
            "TIME" -> VIEW_TYPE2
            "DATETIME" -> VIEW_TYPE3
            "DECIMAL" -> VIEW_TYPE4
            "INTEGER" -> VIEW_TYPE4
            "BOOLEAN" -> VIEW_TYPE5
            "TEXT" -> VIEW_TYPE6
            else -> VIEW_TYPE8
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): MyViewHolder {
        val resourceId = when (viewType) {
            VIEW_TYPE0 -> R.layout.layout_report_date_picker
            VIEW_TYPE1 -> R.layout.layout_date_picker
            VIEW_TYPE2 -> R.layout.layout_time_picker
            VIEW_TYPE3 -> R.layout.layout_date_time_picker
            VIEW_TYPE4 -> R.layout.layout_quantity_selecter
            VIEW_TYPE5 -> R.layout.layout_switch
            VIEW_TYPE6 -> R.layout.layout_edittext
            VIEW_TYPE7 -> R.layout.layout_spinner
            else -> R.layout.empty_layout
        }

        return MyViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(resourceId, viewGroup, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val checkList = subProcessList[sequence].checkLists[position]
        when {
            checkList.field_type.equals("BOOLEAN", true) -> {
                holder.switch?.text = checkList.name
                holder.switch?.isChecked = checkList.answer.equals("true", ignoreCase = true)
                holder.switch?.isClickable = false
            }
            checkList.field_type.equals("REPORT DATE", ignoreCase = true) -> {
                holder.tvTitle?.text = checkList.name
                holder.tvReportDate?.text = convertDateToDisplayFormat(checkList.answer)
                holder.etDate?.setText(holder.tvReportDate?.text.toString())
            }
            checkList.field_type.equals("DATE", ignoreCase = true) -> {
                holder.tvTitle?.text = checkList.name
                holder.etDate?.setText(convertDateToDisplayFormat(checkList.answer))
            }
            checkList.field_type.equals("TIME", ignoreCase = true) -> {
                if (previousCheckListHasSameNameAndType(position))
                    return

                // if true, render two different view in a row else only one view in a row
                if (nextCheckListHasSameNameAndType(position)) {
                    val nextCheckList = subProcessList[sequence].checkLists[position + 1]
                    holder.tvTitle?.text = checkList.name
                    holder.tvKeyStartTime?.text = checkList.subtitle
                    holder.tvKeyEndTime?.text = nextCheckList.subtitle
                    holder.etStartTime?.setText(checkList.answer)
                    holder.etEndTime?.setText(nextCheckList.answer)
                } else {
                    holder.tvTitle?.text = checkList.name
                    holder.tvKeyStartTime?.text = activity.getString(R.string.time)
                    holder.etStartTime?.setText(checkList.answer)
                    holder.etEndTime?.setText("")
                    holder.tvKeyEndTime?.visibility = View.INVISIBLE
                    holder.etEndTime?.visibility = View.INVISIBLE
                    holder.imgEndTime?.visibility = View.INVISIBLE
                }
            }
            checkList.field_type.equals("DATETIME", ignoreCase = true) -> {
                holder.tvTitle?.text = checkList.name
                holder.etDate?.setText(convertDateToDisplayFormat(checkList.answer))
                holder.etTime?.setText(checkList.answer.substring(8))
            }

            checkList.getAvailableChoices().isNotEmpty() -> {
                val unit = if (checkList.unit.isNullOrEmpty()) "" else " (${checkList.unit}) "
                holder.tvKeyStartTime?.text = if (checkList.unit.equals("Litre", ignoreCase = true) || checkList.unit.equals("Gram", ignoreCase = true) || checkList.unit.equals("Kilogram", ignoreCase = true)) "Select Weight$unit" else "Select Answer"
                holder.tvTitle?.text = checkList.name

                val arrayAdapter = ArrayAdapter(activity, android.R.layout.simple_spinner_item, listOf(checkList.answer))
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                holder.spinner?.adapter = arrayAdapter
                holder.spinner?.setSelection(arrayAdapter.getPosition(checkList.answer))
                holder.spinner?.isEnabled = false
            }
            checkList.field_type.equals("TEXT", ignoreCase = true) -> {
                holder.tvTitle?.text = checkList.name
                holder.editText?.setText(checkList.answer)
                holder.editText?.isEnabled = false
            }
            checkList.unit == null || checkList.unit.equals("MINUTE", ignoreCase = true) || checkList.unit.equals("SECOND", ignoreCase = true) ||
            checkList.unit.equals("Litre", ignoreCase = true) || checkList.unit.equals("Kilogram", ignoreCase = true) || checkList.unit.equals("Gram", ignoreCase = true) -> {
                holder.tvTitle?.text = checkList.name
                holder.tvQty?.text = checkList.answer
            }
            checkList.unit.equals("Celcius", ignoreCase = true) -> {
                holder.tvTitle?.text = checkList.name
                holder.tvQty?.text = checkList.answer.plus("°")
            }
        }
    }

    override fun getItemCount(): Int = subProcessList[sequence].checkLists.size


    private fun previousCheckListHasSameNameAndType(position: Int): Boolean {
        return try {
            val checkList = subProcessList[sequence].checkLists[position]
            val previousCheckList = subProcessList[sequence].checkLists[position - 1]
            checkList.name.equals(previousCheckList.name, ignoreCase = true) &&
                    checkList.field_type.equals(previousCheckList.field_type, ignoreCase = true)
        } catch (e: Exception) {
            false
        }
    }

    private fun nextCheckListHasSameNameAndType(position: Int): Boolean {
        return try {
            val checkList = subProcessList[sequence].checkLists[position]
            val nextCheckList = subProcessList[sequence].checkLists[position + 1]
            checkList.name.equals(nextCheckList.name, ignoreCase = true) &&
                    checkList.field_type.equals(nextCheckList.field_type, ignoreCase = true)
        } catch (e: Exception) {
            false
        }
    }

    fun showPreviousStage() {
        if (sequence == 0) {
            fragment.showProgress(subProcessList, sequence)
            activity.onBackPressed()
        }

        if (sequence > 0) {
            --sequence
            fragment.showProgress(subProcessList, sequence)

            notifyDataSetChanged()
        }
    }

    fun showNextStage() {

        // if current subProcess is not any but not last subProcess then, show next process
        if (sequence < subProcessList.size - 1) {
            ++sequence
            fragment.showProgress(subProcessList, sequence)
            notifyDataSetChanged()
            return
        }

        // if last subProcess, then go to back screen.
        if (sequence == subProcessList.size - 1) {
            fragment.showProgress(subProcessList, sequence)
            activity.onBackPressed()
        }
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var switch: SwitchCompat? = view.switch1
        var tvTitle: TextView? = view.tv_title
        var tvReportDate: TextView? = view.tv_report_date
        var etDate: EditText? = view.et_date
        var etTime: EditText? = view.et_time

        var tvKeyStartTime: TextView? = view.tv_title_start_time
        var tvKeyEndTime: TextView? = view.tv_title_end_time
        var etStartTime: EditText? = view.et_start_time
        var etEndTime: EditText? = view.et_end_time
        var imgEndTime: ImageView? = view.img_end_time
        var tvQty: TextView? = view.tv_qty
        var spinner: Spinner? = view.spinner_answer
        var editText: EditText? = view.edittext
    }
}