package com.example.cdpackaging.reports

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.cdpackaging.R
import com.example.cdpackaging.room.entity.ReportsProduct
import kotlinx.android.synthetic.main.cell_reports_process.view.*


class ReportAdapter(
    private val reportsFragment: ReportsFragment,
    private val productList: List<ReportsProduct>
) :
    RecyclerView.Adapter<ReportAdapter.ViewHolder>() {

    override fun getItemCount() = productList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cell_reports_process, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val product = productList[position]
        //holder.img_item.loadImageWithCoil(product.image_url)
        holder.tv_item.text = product.product
        holder.tv_time.text = "${product.start_time} - ${product.end_time}"


        /*if (product.process_status == "Done" && product.process_sync == false)
            holder.img_status.setBackgroundResource(R.drawable.bg_circular_red)
        else if (product.process_status == "Done" && product.process_sync == true)
            holder.img_status.setBackgroundResource(R.drawable.bg_circular_green)*/

        holder.root_process.setOnClickListener { reportsFragment.showSubProcess(product) }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var root_process: ConstraintLayout = itemView.root
        var img_item: ImageView = itemView.img_item
        var tv_item: TextView = itemView.tv_product
        var tv_time: TextView = itemView.tv_time
        var img_status: ImageView = itemView.img_status
    }
}