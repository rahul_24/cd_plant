package com.example.cdpackaging.reports

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cdpackaging.HomeActivity
import com.example.cdpackaging.R
import com.example.cdpackaging.network.PlantApi
import com.example.cdpackaging.room.DataConverter
import com.example.cdpackaging.room.entity.ReportsChecklist
import com.example.cdpackaging.room.entity.ReportsProduct
import com.example.cdpackaging.room.relation.ReportsSubProcessWithCheckList
import com.example.cdpackaging.room.view_model.ProcessViewModel
import com.example.cdpackaging.utils.*
import com.example.cdpackaging.utils.Utility.convertDateToDisplayFormat
import com.example.cdpackaging.utils.Utility.getServerFormatCurrentDate
import com.example.cdpackaging.utils.Utility.getToken
import com.example.cdpackaging.utils.Utility.safeApi
import kotlinx.android.synthetic.main.fragment_reports.view.*
import kotlinx.coroutines.flow.collect
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*


class ReportsFragment : Fragment() {

    private lateinit var imageBack: ImageView
    private lateinit var tvProcess: TextView
    private lateinit var tvSubProcess: TextView
    private lateinit var tvProgress: TextView
    private lateinit var textViewNoRecord: TextView
    private lateinit var recyclerView: RecyclerView
    private lateinit var btnPrevious: TextView
    private lateinit var btnNext: Button
    private lateinit var imageFilter: ImageView
    private lateinit var imageRefresh: ImageView
    private lateinit var layoutFilter: LinearLayout
    private lateinit var tvRecent: TextView
    private lateinit var tvCurrentMonth: TextView
    private lateinit var tvPreviousMont: TextView
    private var filterScreen = RECENT_SCREEN
    private var silentRefreshOnce = true
    private var isFilterMenuDisplayed = true

    private var selectedDate: String = ""
    private lateinit var product: ReportsProduct
    private var currentScreen: String = PROCESS_SCREEN
    private val processViewModel: ProcessViewModel by viewModel()
    private var isLoading = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.fragment_reports, container, false)
        with(view) {
            imageBack = img_back
            imageFilter = img_filter
            imageRefresh = img_refresh
            layoutFilter = layout_filter
            tvRecent = tv_recent
            tvCurrentMonth = tv_current_month
            tvPreviousMont = tv_previous_month

            tvProcess = tv_process
            tvSubProcess = tv_sub_process
            tvProgress = tv_progress
            textViewNoRecord = tv_no_record
            recyclerView = recycler_reports
            btnPrevious = btn_previous
            btnNext = btn_next
        }

        selectedDate = getServerFormatCurrentDate()
        manageActionListener()
        showReportsDate()

        return view
    }


    private fun manageActionListener() {
        imageBack.setOnClickListener {
            val activity = context as HomeActivity
            activity.onBackPressed()
        }

        imageRefresh.setOnClickListener {
            loadNextPage(true)
        }

        imageFilter.setOnClickListener {
            if (currentScreen == REPORTS_DATE_SCREEN) {
                val visibility =
                    if (layoutFilter.visibility == View.VISIBLE) View.GONE else View.VISIBLE
                layoutFilter.visibility = visibility
                isFilterMenuDisplayed = visibility == View.VISIBLE
            }
        }

        tvRecent.setOnClickListener {
            filterScreen = tvRecent.text.toString()
            tvRecent.setBackgroundResource(R.drawable.bg_rounded_blue)
            tvCurrentMonth.setBackgroundResource(R.drawable.bg_rounded_grey)
            tvPreviousMont.setBackgroundResource(R.drawable.bg_rounded_grey)

            tvRecent.setTextColor(resources.getColor(R.color.white))
            tvCurrentMonth.setTextColor(resources.getColor(R.color.blue))
            tvPreviousMont.setTextColor(resources.getColor(R.color.blue))

            //recyclerView.adapter = ReportsDateAdapter(this, getCurrentDate())
            showReportsDate()
        }

        tvCurrentMonth.setOnClickListener {
            filterScreen = tvCurrentMonth.text.toString()
            tvRecent.setBackgroundResource(R.drawable.bg_rounded_grey)
            tvCurrentMonth.setBackgroundResource(R.drawable.bg_rounded_blue)
            tvPreviousMont.setBackgroundResource(R.drawable.bg_rounded_grey)

            tvRecent.setTextColor(resources.getColor(R.color.blue))
            tvCurrentMonth.setTextColor(resources.getColor(R.color.white))
            tvPreviousMont.setTextColor(resources.getColor(R.color.blue))

            textViewNoRecord.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
            if (recyclerView.itemDecorationCount == 0)
                recyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            recyclerView.adapter = ReportsDateAdapter(this, getAllDaysOfMonth())
        }

        tvPreviousMont.setOnClickListener {
            filterScreen = tvPreviousMont.text.toString()
            tvRecent.setBackgroundResource(R.drawable.bg_rounded_grey)
            tvCurrentMonth.setBackgroundResource(R.drawable.bg_rounded_grey)
            tvPreviousMont.setBackgroundResource(R.drawable.bg_rounded_blue)

            tvRecent.setTextColor(resources.getColor(R.color.blue))
            tvCurrentMonth.setTextColor(resources.getColor(R.color.blue))
            tvPreviousMont.setTextColor(resources.getColor(R.color.white))

            textViewNoRecord.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
            if (recyclerView.itemDecorationCount == 0)
                recyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            recyclerView.adapter = ReportsDateAdapter(this, getAllDaysOfMonth(false))
        }

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

                if (dy > 0 && currentScreen == PROCESS_SCREEN) { //check for scroll down
                    val mLayoutManager = (recyclerView.layoutManager!! as LinearLayoutManager)
                    val visibleItemCount = mLayoutManager.childCount
                    val totalItemCount = mLayoutManager.itemCount
                    val firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition()

                    if (!isLoading) {
                        if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                            loadNextPage()
                        }
                    }
                }
            }
        })
    }

    private fun getCurrentDate(): List<ReportsProduct> {
        val datesList = mutableListOf<ReportsProduct>()

        val currentDate = Utility.getCurrentDate()
        datesList.add(ReportsProduct(currentDate, null, null, null, 0, "", "", "", null))
        return datesList
    }

    private fun getAllDaysOfMonth(showCurrentMonth: Boolean = true): List<ReportsProduct> {
        val datesList = mutableListOf<ReportsProduct>()

        val calendar: Calendar = Calendar.getInstance().clone() as Calendar
        calendar.set(Calendar.DAY_OF_MONTH, 1)
        if (!showCurrentMonth)
            calendar.add(Calendar.MONTH, -1)

        val daysCount = calendar.getActualMaximum(Calendar.DAY_OF_MONTH)

        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

        while (datesList.size < daysCount) {
            val date = simpleDateFormat.format(calendar.time)
            datesList.add(ReportsProduct(date, null, null, null, 0, "", "", "", null))
            calendar.add(Calendar.DAY_OF_MONTH, 1)
        }

        return datesList
    }


    private fun showReportsDate() {
        currentScreen = REPORTS_DATE_SCREEN
        tvProcess.text = getString(R.string.reports)
        tvSubProcess.text = ""
        imageFilter.visibility = View.VISIBLE
        if (isFilterMenuDisplayed) layoutFilter.visibility = View.VISIBLE
        imageRefresh.clearAnimation()
        imageRefresh.visibility = View.GONE

        if (filterScreen == CURRENT_MONTH_SCREEN || filterScreen == PREVIOUS_MONTH_SCREEN) {
            textViewNoRecord.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
            recyclerView.alpha = 1.0f

            recyclerView.adapter =
                ReportsDateAdapter(this, getAllDaysOfMonth(filterScreen == CURRENT_MONTH_SCREEN))
            if (recyclerView.itemDecorationCount == 0)
                recyclerView.addItemDecoration(
                    DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
                )
            return
        }

        processViewModel.getReportsDate().observe(requireActivity(), {
            if (currentScreen == REPORTS_DATE_SCREEN && filterScreen == RECENT_SCREEN) {
                if (it.isNullOrEmpty()) {
                    textViewNoRecord.visibility = View.GONE
                    recyclerView.visibility = View.GONE
                    loadFirstPage()               // show loader

                } else if (context != null) {
                    textViewNoRecord.visibility = View.GONE
                    recyclerView.visibility = View.VISIBLE
                    recyclerView.alpha = 1.0f

                    recyclerView.adapter = ReportsDateAdapter(this, it)
                    if (recyclerView.itemDecorationCount == 0)
                        recyclerView.addItemDecoration(
                            DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
                        )
                }
            }
        })
    }

    fun showProcess(reportsDate: String) {
        currentScreen = PROCESS_SCREEN
        this.selectedDate = reportsDate
        tvProcess.text = context?.getString(R.string.reports)
        imageFilter.visibility = View.GONE
        layoutFilter.visibility = View.GONE
        tvSubProcess.text = convertDateToDisplayFormat(reportsDate)

        processViewModel.getReportsProcess(reportsDate).observe(this, {
            if (currentScreen != PROCESS_SCREEN || selectedDate != reportsDate)
                return@observe

            if (it.isNullOrEmpty()) {
                textViewNoRecord.visibility = View.GONE
                recyclerView.visibility = View.GONE
                silentRefreshOnce = false
                loadFirstPage()

            } else if (context != null) {
                imageRefresh.visibility = View.VISIBLE
                textViewNoRecord.visibility = View.GONE
                recyclerView.visibility = View.VISIBLE
                recyclerView.alpha = 1.0f

                recyclerView.adapter = ReportAdapter(this, it.toMutableList())
                if (recyclerView.itemDecorationCount == 0)
                    recyclerView.addItemDecoration(
                        DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
                    )

                if (silentRefreshOnce && selectedDate == getServerFormatCurrentDate()) {
                    silentRefreshOnce = false
                    silentLoadFirstPage()     // don't show loader
                }
            }
        })
    }

    fun showSubProcess(reportsProduct: ReportsProduct) {
        currentScreen = SUB_PROCESS_SCREEN
        imageRefresh.clearAnimation()
        imageRefresh.visibility = View.GONE

        this.product = reportsProduct
        val processId = reportsProduct.process_id
        tvProcess.text = reportsProduct.product
        tvSubProcess.text = ""

        processViewModel.getReportsSubProcess(processId, reportsProduct.start_time).observe(this, {
            if (currentScreen != SUB_PROCESS_SCREEN)
                return@observe

            if (it.isNullOrEmpty()) {
                textViewNoRecord.visibility = View.VISIBLE
                recyclerView.visibility = View.GONE

            } else {
                textViewNoRecord.visibility = View.GONE
                recyclerView.visibility = View.VISIBLE
                recyclerView.alpha = 1.0f

                recyclerView.adapter = ReportsSubAdapter(this, it, processId)
                if (recyclerView.itemDecorationCount == 0)
                    recyclerView.addItemDecoration(
                        DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
                    )
            }
        })
    }

    fun showCheckList(process_id: Int, startTime: String, subProcess_index: Int) {
        currentScreen = CHECKLIST_SCREEN
        imageRefresh.visibility = View.GONE

        var autoUpdate = true

        processViewModel.getReportsCheckList(process_id, startTime).observe(this, {
            if (currentScreen != CHECKLIST_SCREEN)
                return@observe

            if (it.isNullOrEmpty()) {
                textViewNoRecord.visibility = View.VISIBLE
                recyclerView.visibility = View.GONE

            } else if (it.isNotEmpty() && autoUpdate) {
                textViewNoRecord.visibility = View.GONE
                recyclerView.visibility = View.VISIBLE

                val list = mutableListOf<ReportsSubProcessWithCheckList>()
                it.forEach { outer ->
                    val checkList = mutableListOf<ReportsChecklist>()
                    checkList.addAll(outer.checkLists.filter { it.start_time == startTime})
                    list.add(ReportsSubProcessWithCheckList(outer.subProcess, checkList))
                }

                showProgress(list, subProcess_index)
                val adapter = ReportsCheckAdapter(this, list, subProcess_index)
                recyclerView.alpha = 0.6f
                recyclerView.adapter = adapter
                if (recyclerView.itemDecorationCount > 0)
                    recyclerView.removeItemDecorationAt(0)

                btnPrevious.visibility = View.VISIBLE
                btnNext.visibility = View.VISIBLE
                btnPrevious.setOnClickListener { adapter.showPreviousStage() }
                btnNext.setOnClickListener { adapter.showNextStage() }
                autoUpdate = false
            }
        })
    }


    private fun loadFirstPage() {
        Log.d("ApiCall", "ReportsFragment::loadFirstPage ($selectedDate)")

        processViewModel.launchDataLoad {
            val reportDate = processViewModel.getNextReportsProcessUrl(selectedDate)
            if (reportDate != null && reportDate.next.isNullOrEmpty()) {
                textViewNoRecord.visibility = View.VISIBLE
                imageRefresh.visibility = View.GONE
                return@launchDataLoad
            }

            safeApi {
                PlantApi.RetroFitService.getReports(getToken(), selectedDate, "1")
            }.collect { response ->
                when (response) {
                    is Result.Loading -> {
                        CustomProgressDialog.show(context)
                    }
                    is Result.Error -> {
                        CustomProgressDialog.dismiss()
                        context?.showToast(response.code, response.detail)
                    }
                    is Result.NetworkError -> {
                        CustomProgressDialog.dismiss()
                        context?.showToast()
                    }
                    is Result.Success -> {
                        CustomProgressDialog.dismiss()
                        val reportsDate =
                            DataConverter().getReportsDate(response.data, selectedDate)
                        val reportsProduct = DataConverter().getReportsProduct(response.data)
                        val reportsSubProcess = DataConverter().getReportsSubProcess(response.data)
                        val reportsCheckList = DataConverter().getReportsCheckList(response.data)
                        processViewModel.addReportsData(
                            reportsDate,
                            reportsProduct,
                            reportsSubProcess,
                            reportsCheckList
                        )

                        if (reportsProduct.isEmpty()) {
                            processViewModel.addReportsDate(selectedDate)
                            textViewNoRecord.visibility = View.VISIBLE
                            imageRefresh.visibility = View.GONE
                        } else {
                            textViewNoRecord.visibility = View.GONE
                            imageRefresh.visibility = View.GONE
                        }
                    }
                }
            }
        }
    }

    private fun silentLoadFirstPage() {
        Log.d("ApiCall", "ReportsFragment::silentLoadFirstPage ($selectedDate)")

        processViewModel.launchDataLoad {
            safeApi {
                PlantApi.RetroFitService.getReports(getToken(), selectedDate, "1")
            }.collect { response ->
                when (response) {
                    is Result.Loading -> {
                    }
                    is Result.Error -> {
                        context?.showToast(response.code, response.detail)
                    }
                    is Result.NetworkError -> {
                    }
                    is Result.Success -> {
                        Log.d("ApiCall", "ReportsFragment::silentLoadFirstPage")
                        //val reportsDate = DataConverter().getReportsDate(response.data, selectedDate)
                        val reportsProduct = DataConverter().getReportsProduct(response.data)
                        val reportsSubProcess = DataConverter().getReportsSubProcess(response.data)
                        val reportsCheckList = DataConverter().getReportsCheckList(response.data)
                        processViewModel.silentAddReportsData(
                            //reportsDate,
                            reportsProduct,
                            reportsSubProcess,
                            reportsCheckList
                        )
                    }
                }
            }
        }
    }

    private fun loadNextPage(showMessage : Boolean = false) {
        Log.d("ApiCall", "ReportsFragment::loadNextPage ($selectedDate)")

        processViewModel.launchDataLoad {
            val reportDate = processViewModel.getNextReportsProcessUrl(selectedDate)
            if (reportDate == null) {
                loadFirstPage()
                return@launchDataLoad
            }
            if (reportDate.next.isNullOrEmpty()) {
                isLoading = false
                if (showMessage) context?.showToast("No more records to show")
                return@launchDataLoad
            }

            safeApi {
                PlantApi.RetroFitService.getNextReports(getToken(), reportDate.next!!)
            }.collect { response ->
                when (response) {
                    is Result.Loading -> {
                        isLoading = true
                        imageRefresh.startAnimation(AnimationUtils.loadAnimation(context, R.anim.rotate))
                    }
                    is Result.Error -> {
                        isLoading = false
                        imageRefresh.clearAnimation()
                        context?.showToast(response.code, response.detail)
                    }
                    is Result.NetworkError -> {
                        isLoading = false
                        imageRefresh.clearAnimation()
                        context?.showToast()
                    }
                    is Result.Success -> {
                        isLoading = false
                        imageRefresh.clearAnimation()

                        val reportsDate = DataConverter().getReportsDate(response.data, selectedDate)
                        val reportsProduct = DataConverter().getReportsProduct(response.data)
                        val reportsSubProcess = DataConverter().getReportsSubProcess(response.data)
                        val reportsCheckList = DataConverter().getReportsCheckList(response.data)
                        processViewModel.addReportsData(
                            reportsDate,
                            reportsProduct,
                            reportsSubProcess,
                            reportsCheckList
                        )
                    }
                }
            }
        }
    }


    fun showProgress(subProcessWithCheckList: List<ReportsSubProcessWithCheckList>, sequence: Int) {
        when {
            sequence == 0 -> {
                btnPrevious.text = getString(R.string.close)
                btnNext.text = getString(R.string.next)
            }
            sequence < subProcessWithCheckList.size - 1 -> {
                btnPrevious.text = getString(R.string.previous)
                btnNext.text = getString(R.string.next)
            }
            sequence == subProcessWithCheckList.size - 1 -> {
                btnPrevious.text = getString(R.string.previous)
                btnNext.text = getString(R.string.close)
            }
        }

        tvSubProcess.text = subProcessWithCheckList[sequence].subProcess.sub_process
        tvProgress.text = getString(R.string.progress, sequence + 1, subProcessWithCheckList.size)
    }

    private companion object {
        private const val REPORTS_DATE_SCREEN = "ReportsDate"
        private const val PROCESS_SCREEN = "Process"
        private const val SUB_PROCESS_SCREEN = "SubProcess"
        private const val CHECKLIST_SCREEN = "CheckList"

        private const val RECENT_SCREEN = "RECENT"
        private const val CURRENT_MONTH_SCREEN = "CURRENT MONTH"
        private const val PREVIOUS_MONTH_SCREEN = "PREVIOUS MONTH"
    }

    fun onBackPressed(): Boolean {
        //Screen 1
        if (currentScreen == REPORTS_DATE_SCREEN) {
            return false
        }

        //Screen 2
        if (currentScreen == PROCESS_SCREEN) {
            showReportsDate()
            return true
        }

        //Screen 3
        if (currentScreen == SUB_PROCESS_SCREEN) {
            showProcess(selectedDate)
            return true
        }

        //Screen 4
        if (currentScreen == CHECKLIST_SCREEN) {
            tvSubProcess.text = ""
            tvProgress.text = ""
            btnPrevious.visibility = View.GONE
            btnNext.visibility = View.GONE
            showSubProcess(product)
            return true
        }

        return true
    }
}