package com.example.cdpackaging.reports

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.cdpackaging.R
import com.example.cdpackaging.room.entity.ReportsSubProcess
import kotlinx.android.synthetic.main.cell_sub_process.view.*


class ReportsSubAdapter(
    private val fragment: ReportsFragment,
    private val subProcessList: List<ReportsSubProcess>,
    private val process_id: Int
) :
    RecyclerView.Adapter<ReportsSubAdapter.ViewHolder>() {

    override fun getItemCount() = subProcessList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cell_sub_process, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val reportsSubProcess = subProcessList[position]
        holder.tv_sequence.text = (position + 1).toString()
        holder.tv_sequence.setBackgroundResource(R.drawable.bg_rounded_green)

        holder.tv_sub_process.text = reportsSubProcess.sub_process
        holder.tv_status.text = "Done"
        holder.tv_status.setTextColor(holder.tv_status.context.resources.getColor(R.color.green))
        holder.root.setOnClickListener { fragment.showCheckList(process_id, reportsSubProcess.start_time, position) }
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var root: ConstraintLayout = itemView.root
        var tv_sequence: TextView = itemView.tv_sequence
        var tv_sub_process: TextView = itemView.tv_sub_process
        var tv_status: TextView = itemView.tv_status
    }
}