package com.example.cdpackaging.reports

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.cdpackaging.R
import com.example.cdpackaging.room.entity.ReportsProduct
import com.example.cdpackaging.utils.Utility
import kotlinx.android.synthetic.main.cell_reports_date.view.*


class ReportsDateAdapter(
    private val reportsFragment: ReportsFragment,
    private val productList: List<ReportsProduct>
) :
    RecyclerView.Adapter<ReportsDateAdapter.ViewHolder>() {

    override fun getItemCount() = productList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cell_reports_date, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val product = productList[position]
        holder.tv_item.text = Utility.convertDateToDisplayFormat(product.date)
        //holder.tv_time.text = product.start_time
        holder.root_process.setOnClickListener { reportsFragment.showProcess(product.date) }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var root_process: ConstraintLayout = itemView.root
        var tv_item: TextView = itemView.tv_product
        //var tv_time: TextView = itemView.tv_time
    }
}