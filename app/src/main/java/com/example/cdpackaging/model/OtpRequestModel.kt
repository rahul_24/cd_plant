package com.example.cdpackaging.model


data class OtpRequestModel(
        var mobile: String,
        var timestamp: Long,
        var app_code: String = "plant_app"
)