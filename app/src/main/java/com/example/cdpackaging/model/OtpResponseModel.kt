package com.example.cdpackaging.model

data class OtpResponseModel(
        var id: Int? = null,
        var detail: String? = ""
)