package com.example.cdpackaging.model


data class SubProcessResponse(
    var id: Int,
    var name: String,
    var description: String,
    var sequence: Int,
    var active: Boolean,
    var checklists: List<CheckListResponse>
)


data class CheckListResponse(
    var id: Int,
    var subprocess_id: Int = 0,
    var name: String,
    var subtitle: String,
    var sequence: Int,
    var description: String,
    var field_type: String,
    var choices: List<String>,
    var range: List<String>,
    var unit: String? = null,
    var answer: String? = null,
    var active: Boolean,
    var published: Boolean,
)