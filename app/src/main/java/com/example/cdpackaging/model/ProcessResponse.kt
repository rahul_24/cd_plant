package com.example.cdpackaging.model

import com.example.cdpackaging.room.entity.Product

data class ProcessResponse(
    var id: Int,
    var name: String,
    var description: String,
    var product: Product
)