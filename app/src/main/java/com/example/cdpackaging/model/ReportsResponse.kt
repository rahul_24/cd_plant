package com.example.cdpackaging.model

data class ReportsResponse(
    var count: Int? = null,
    var next: String? = null,
    var previous: String? = null,
    var results: List<Results>? = null,
)

data class Results(
    var date: String,
    var batch_id: String? = null,
    var production_centre: String? = null,
    var product: String? = null,
    var process_id: Int,
    var process: String? = null,
    var start_time: String? = null,
    var end_time: String? = null,
    var user_id: Int? = null,
    var logsheet_details: List<LogsheetDetail>? = null,
)

data class LogsheetDetail(
    var checklist: Checklist? = null,
    var answer: String? = null
)

data class Checklist(
    var id: Int,
    var name: String? = null,
    var subtitle: String? = null,
    var sequence: Int? = null,
    var description: String? = null,
    var field_type: String? = null,
    var sub_process_id: Int,
    var sub_process: String? = null,
    var choices: List<String>,
    var range: List<String>,
    var unit: String? = null,
    )