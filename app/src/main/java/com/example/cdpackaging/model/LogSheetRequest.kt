package com.example.cdpackaging.model

data class LogSheetRequest(
    var date: String,
    var production_centre_id: Int,
    var product_id: Int,
    var process_id: Int,
    var start_time: String,
    var end_time: String,
    var logsheet_details: List<HashMap<String, String>>
)