package com.example.cdpackaging.model

data class ProfileResponse(
    var username: String? = null,
    var name: String? = null,
    var mobile: String? = null,
    var email: String? = null,
    var address: String? = null,
    var production_centre: ProductionCentre? = null
)

data class ProductionCentre(
    var id: Int? = null,
    var name: String? = null,
    var description: String? = null,
    //var plant: List<Any>? = null,
    //var mcc_id: Int? = null,
    //var franchise_id: Int? = null,
    //var warehouse_id: Int? = null
)