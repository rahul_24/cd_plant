package com.example.cdpackaging.model

class VerifyResponseModel {
    var access_token: String = ""
    var role: String? = null
}
