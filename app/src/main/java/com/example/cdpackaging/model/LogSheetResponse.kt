package com.example.cdpackaging.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LogSheetResponse {
    @SerializedName("date")
    @Expose
    var date: String? = null

    @SerializedName("batch_id")
    @Expose
    var batchId: String? = null

    @SerializedName("production_centre_id")
    @Expose
    var productionCentreId: Int? = null

    @SerializedName("product_id")
    @Expose
    var productId: Int? = null

    @SerializedName("shift_id")
    @Expose
    var shiftId: Int? = null

    @SerializedName("process_id")
    @Expose
    var processId: Int? = null

    @SerializedName("start_time")
    @Expose
    var startTime: String? = null

    @SerializedName("end_time")
    @Expose
    var endTime: String? = null

    @SerializedName("user_id")
    @Expose
    var userId: Int? = null

    @SerializedName("logsheet_details")
    @Expose
    var logsheetDetails: List<LogSheetDetail>? = null
}

class LogSheetDetail {
    @SerializedName("checklist_id")
    @Expose
    var checklistId: Int? = null

    @SerializedName("text")
    @Expose
    var text: String? = null

    @SerializedName("boolean")
    @Expose
    var _boolean: Any? = null

    @SerializedName("decimal")
    @Expose
    var decimal: Any? = null

    @SerializedName("date")
    @Expose
    var date: Any? = null

    @SerializedName("time")
    @Expose
    var time: String? = null

    @SerializedName("datetime")
    @Expose
    var datetime: Any? = null
}