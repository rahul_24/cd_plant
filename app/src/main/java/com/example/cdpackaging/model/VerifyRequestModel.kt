package com.example.cdpackaging.model

data class VerifyRequestModel(var id: Int, val otp: String)