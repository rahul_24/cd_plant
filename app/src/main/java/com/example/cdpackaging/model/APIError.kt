package com.example.cdpackaging.model

data class APIError(val detail: String = "")