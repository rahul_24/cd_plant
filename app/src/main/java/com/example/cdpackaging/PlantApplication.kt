package com.example.cdpackaging

import android.app.Application
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelStoreOwner
import androidx.work.*
import com.example.cdpackaging.di.viewModelModule
import com.example.cdpackaging.login.LoginActivity
import com.example.cdpackaging.room.view_model.ProcessViewModel
import com.example.cdpackaging.utils.AppSettings
import com.example.cdpackaging.workManager.DailyWorker
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.context.startKoin
import java.util.concurrent.TimeUnit

/**
 * Created by Rahul on 07-12-20.
 */
class PlantApplication : Application() {
    private var mAppSettings: AppSettings? = null
    val applicationScope = CoroutineScope(SupervisorJob())

    override fun onCreate() {
        super.onCreate()
        appInstance = this

        //A kotlin dependency injector
        startKoin {
            androidContext(applicationContext)
            modules(viewModelModule)
        }
    }


    fun startWorker(showNotification: Boolean = false, isManualSync : Boolean = false) {
        val constraints =
            Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()

        // According to Kotlin Docs- WorkManager has to be set minimum repeat interval of 15 minutes. (Below it, won't work)
        val periodicWorkRequest =
            PeriodicWorkRequestBuilder<DailyWorker>(15, TimeUnit.MINUTES)
                .addTag(JOB_REQUEST_TAG)
                .setInputData(
                    Data.Builder().putBoolean("showNotification", showNotification)
                        .putBoolean("isManualSync", isManualSync)
                        .build()
                )
                .setConstraints(constraints).build()

        WorkManager.getInstance(applicationContext).enqueueUniquePeriodicWork(
            UNIQUE_JOB_TAG,
            ExistingPeriodicWorkPolicy.KEEP,
            periodicWorkRequest
        )
    }

    fun stopWorker() {
        WorkManager.getInstance(applicationContext).cancelAllWorkByTag(JOB_REQUEST_TAG)
    }

    fun restartWorker(isManualSync : Boolean = false){
        stopWorker()
        startWorker(isManualSync = isManualSync)
    }

    fun getAppSettings(): AppSettings = mAppSettings ?: AppSettings()

    companion object {
        const val UNIQUE_JOB_TAG = "UniqueJob"
        const val JOB_REQUEST_TAG = "JobRequest"
        lateinit var appInstance: PlantApplication

        fun logout(activity: AppCompatActivity) {
            appInstance.getAppSettings().clearSharedPreferences()
            appInstance.stopWorker()

            val processViewModel: ProcessViewModel by (activity as ViewModelStoreOwner).viewModel()
            processViewModel.clearAllTables()

            val intent = Intent(activity, LoginActivity::class.java)
            activity.startActivity(intent)
            activity.finish()
        }

        fun logout() {
            appInstance.getAppSettings().clearSharedPreferences()
            appInstance.stopWorker()
            ProcessViewModel(appInstance).clearAllTables()

            val intent = Intent(appInstance, LoginActivity::class.java)
            intent.flags = FLAG_ACTIVITY_NEW_TASK
            appInstance.startActivity(intent)
        }
    }
}