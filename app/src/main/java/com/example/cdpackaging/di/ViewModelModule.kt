package com.example.cdpackaging.di

import com.example.cdpackaging.room.view_model.CustomViewModel
import com.example.cdpackaging.room.view_model.ProcessViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { ProcessViewModel(get()) }
    viewModel { CustomViewModel(get()) }
}