package com.example.cdpackaging.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.example.cdpackaging.HomeActivity
import com.example.cdpackaging.PlantApplication
import com.example.cdpackaging.R
import com.example.cdpackaging.room.view_model.ProcessViewModel
import com.example.cdpackaging.utils.hasNetwork
import com.example.cdpackaging.utils.showToast
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import org.koin.android.viewmodel.ext.android.viewModel

class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        val processViewModel: ProcessViewModel by viewModel()
        processViewModel.getAllProcess().observe(requireActivity(), {
            if (it.isNullOrEmpty()){
                if(activity is HomeActivity) (activity as HomeActivity).fetchProfile()
            } else {
                val processAdapter = ProcessAdapter(it)
                view.recycler_process.layoutManager = GridLayoutManager(context, 2)
                view.recycler_process.adapter = processAdapter
                view.tv_name.text = PlantApplication.appInstance.getAppSettings().name.plus("!")
            }
        })

        view.tv_edit.setOnClickListener {
            if (activity is HomeActivity && view.recycler_process.adapter == null || view.recycler_process.adapter?.itemCount == 0) {
                if (!activity!!.hasNetwork(false)) {
                    activity!!.showToast()
                    return@setOnClickListener
                }
                (activity as HomeActivity).fetchProcess()
            }
            it.visibility = View.GONE
            img_task.visibility = View.GONE
            img_item.visibility = View.GONE
            tv_product.visibility = View.GONE
            tv_process.visibility = View.GONE
            btn_begin_task.visibility = View.GONE
            view.recycler_process.visibility = View.VISIBLE
        }
        view.btn_begin_task.setOnClickListener {
            if (tv_product.text.toString().isEmpty()) {
                view.tv_edit.performClick()
            } else {
                val intent = Intent(context, SubProcessActivity::class.java)
                intent.putExtra("product_id", PlantApplication.appInstance.getAppSettings().productId)
                intent.putExtra("process_id", PlantApplication.appInstance.getAppSettings().processId)
                intent.putExtra("process_name", PlantApplication.appInstance.getAppSettings().processName)
                startActivity(intent)
            }
        }

        return view
    }

    override fun onResume() {
        super.onResume()

        val product = PlantApplication.appInstance.getAppSettings().productName
        val process = PlantApplication.appInstance.getAppSettings().processName
        if (product.isNotEmpty()) {
            tv_product.text = product
            tv_process.text = process
            img_item.setImageResource(R.drawable.paneer)
            img_item.setBackgroundResource(R.drawable.bg_rounded_trans)
            tv_edit.text = getString(R.string.change)
        }
    }

    private fun showHomeScreen() {
        recycler_process.visibility = View.GONE
        tv_edit.visibility = View.VISIBLE
        img_task.visibility = View.VISIBLE
        btn_begin_task.visibility = View.VISIBLE
        if (tv_product.text.toString().isNotEmpty()) {
            img_item.visibility = View.VISIBLE
            tv_product.visibility = View.VISIBLE
            tv_process.visibility = View.VISIBLE
        }
    }

    fun onBackPressed(): Boolean {
        try {
            if (btn_begin_task.visibility == View.VISIBLE) {
                return false
            }
            showHomeScreen()
            return true
        } catch (e: Exception) {
            return false
        }
    }
}