package com.example.cdpackaging.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.cdpackaging.HomeActivity
import com.example.cdpackaging.PlantApplication
import com.example.cdpackaging.R
import com.example.cdpackaging.model.LogSheetRequest
import com.example.cdpackaging.network.PlantApi
import com.example.cdpackaging.room.DataConverter
import com.example.cdpackaging.room.view_model.ProcessViewModel
import com.example.cdpackaging.utils.*
import com.example.cdpackaging.utils.Utility.getToken
import com.example.cdpackaging.utils.Utility.safeApi
import com.google.gson.Gson
import kotlinx.android.synthetic.main.layout_process_complete.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.collections.HashMap

class ProcessCompleteActivity : AppCompatActivity() {

    private val processViewModel: ProcessViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_process_complete)

        tv_process.text = PlantApplication.appInstance.getAppSettings().processName
        tv_name.text = PlantApplication.appInstance.getAppSettings().name.plus("!")

        img_back.setOnClickListener { btn_home.performClick() }
        btn_home.setOnClickListener {
            startActivity(Intent(this, HomeActivity::class.java))
            finish()
        }
        sendLogSheet()
    }


    private fun sendLogSheet() {
        //processViewModel.launchDataLoad { }
        //supervisorScope {  }
        PlantApplication.appInstance.applicationScope.launch{
            try {
                val userProfile = processViewModel.getUserProfile()
                val process_id = intent.getIntExtra("process_id", 0)
                val product_id = intent.getIntExtra("product_id", 0)
                val process = processViewModel.getProcessStartEndTime(process_id)
                val allCheckList = processViewModel.getProcessCheckList(process_id)

                var date = ""
                val detailsList = mutableListOf<HashMap<String, String>>()
                allCheckList.forEachIndexed { index, checkList ->
                    if (index == 0 && checkList.id == 0) {
                        date = checkList.answer
                    } else {
                        val hashMap = HashMap<String, String>()
                        hashMap["checklist_id"] = checkList.id.toString()
                        val field_type = if (checkList.field_type.equals("INTEGER", ignoreCase = true) ) "decimal" else checkList.field_type.toLowerCase(Locale.getDefault())
                        hashMap[field_type] = checkList.answer
                        detailsList.add(hashMap)
                    }
                }

                if (userProfile?.centre_id == null || process.process_start_time == null || process.process_end_time == null) {
                    fetchProfile()
                    return@launch
                }

                val logSheetRequest =
                    LogSheetRequest(
                        date, userProfile.centre_id!!, product_id, process_id,
                        process.process_start_time ?: Utility.getCurrentTime(),
                        process.process_end_time ?: Utility.getCurrentTime(),
                        detailsList
                    )
                Log.d("LogSheetRequest", Gson().toJson(logSheetRequest))
                Log.d("ApiCall", "ProcessCompletedActivity::sendLogSheet")

                safeApi {
                    PlantApi.RetroFitService.sendLogSheet(getToken(), logSheetRequest)
                }.collect { response ->
                    when (response) {
                        is Result.Loading -> {
                            //CustomProgressDialog.show(this)
                        }
                        is Result.Error -> {
                            //CustomProgressDialog.dismiss()
                            showDismissDialog("Error (${response.code})", response.detail!!)
                            PlantApplication.appInstance.restartWorker()
                        }
                        is Result.NetworkError -> {
                            //CustomProgressDialog.dismiss()
                            showToast()
                        }
                        is Result.Success -> {
                            //CustomProgressDialog.dismiss()
                            //showToast("Success")
                            val count = processViewModel.updateProcessSyncDone(process_id)
                            if (count > 0){
                                PlantApplication.appInstance.restartWorker()
                            }
                        }
                    }
                }

            }catch (e : Exception){
                e.printStackTrace()
            }
        }
    }

    private fun fetchProfile() {
        Log.d("ApiCall", "ProcessCompletedActivity::fetchProfile")

        processViewModel.launchDataLoad {
            safeApi {
                PlantApi.RetroFitService.getProfile(getToken())
            }.collect { response ->
                when (response) {
                    is Result.Loading -> {}
                    is Result.Error -> {
                        showToast(response.code, response.detail)
                    }
                    is Result.NetworkError -> {
                        showToast()
                    }
                    is Result.Success -> {
                        processViewModel.addUserProfile(DataConverter().getUserProfile(response.data))
                        PlantApplication.appInstance.getAppSettings().prodCentreId = response.data.production_centre?.id ?: -1
                        PlantApplication.appInstance.restartWorker()
                    }
                }
            }
        }
    }
}