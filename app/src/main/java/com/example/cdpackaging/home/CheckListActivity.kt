package com.example.cdpackaging.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cdpackaging.R
import com.example.cdpackaging.room.relation.SubProcessWithCheckList
import com.example.cdpackaging.room.view_model.ProcessViewModel
import com.example.cdpackaging.utils.Utility.getCurrentTime
import com.example.cdpackaging.utils.Utility.hideKeyboard
import kotlinx.android.synthetic.main.layout_check_list.*
import org.koin.android.viewmodel.ext.android.viewModel


class CheckListActivity : AppCompatActivity() {

    private var processId = 0
    private val processViewModel: ProcessViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_check_list)

        val productId = intent.getIntExtra("product_id", 0)
        processId = intent.getIntExtra("process_id", 0)
        val processName = intent.getStringExtra("process_name") ?: ""
        val subProcessIndex = intent.getIntExtra("subprocess_index", 0)
        tv_process.text = processName

        var updateOnce = true
        processViewModel.getAllCheckList(processId).observe(this, {
            val list = mutableListOf<SubProcessWithCheckList>()
            it?.forEach {
                val checkList = it.checkLists.filter { it.active && it.published }
                list.add(SubProcessWithCheckList(it.subProcess, checkList))
            }

            if (list.isNotEmpty() && updateOnce) {
                val adapter = CheckListAdapter(
                    this,
                    list,
                    processId,
                    productId,
                    subProcessIndex,
                    processViewModel
                )
                recycler_stage_detail.adapter = adapter
                recycler_stage_detail.layoutManager = LinearLayoutManager(this)

                img_back.setOnClickListener { adapter.showPreviousStage() }
                btn_save_close.setOnClickListener { adapter.saveAndClose() }
                btn_save_next.setOnClickListener {
                    hideKeyboard(it)
                    adapter.showNextStage()
                }

                showCurrentProgress(list, subProcessIndex, subProcessIndex == 0)
                updateOnce = false
            }
        })
    }

    fun showCurrentProgress(
        subProcessWithCheckList: List<SubProcessWithCheckList>,
        subProcess_index: Int,
        updateStartTime: Boolean = false,
        updateEndTime: Boolean = false
    ) {
        if (subProcess_index < subProcessWithCheckList.size) {
            tv_sub_process.text = subProcessWithCheckList[subProcess_index].subProcess.name

            val perSubProcessShare = 100 / subProcessWithCheckList.size
            progressBar.progress = perSubProcessShare * subProcess_index
            tv_progress.text = getString(R.string.percent, progressBar.progress)

            //if first subProcess, update subProcess start time
            if (updateStartTime) {
                processViewModel.updateProcessStartTime(processId, getCurrentTime())
            }

            //if last subProcess, update subProcess end time
            else if (updateEndTime) {
                processViewModel.updateProcessEndTime(processId, getCurrentTime())
            }
        }
    }

    override fun onBackPressed() {
        //img_back.performClick()
        finish()
    }
}