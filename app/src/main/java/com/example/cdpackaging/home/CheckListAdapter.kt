package com.example.cdpackaging.home

import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.cdpackaging.R
import com.example.cdpackaging.room.entity.CheckList
import com.example.cdpackaging.room.relation.SubProcessWithCheckList
import com.example.cdpackaging.room.view_model.ProcessViewModel
import com.example.cdpackaging.utils.Utility.convertDateToServerFormat
import com.example.cdpackaging.utils.Utility.getCurrentDate
import com.example.cdpackaging.utils.showToast
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import kotlinx.android.synthetic.main.edit_text.view.*
import kotlinx.android.synthetic.main.layout_date_picker.view.et_date
import kotlinx.android.synthetic.main.layout_date_picker.view.tv_title
import kotlinx.android.synthetic.main.layout_date_time_picker.view.*
import kotlinx.android.synthetic.main.layout_edittext.view.*
import kotlinx.android.synthetic.main.layout_quantity_selecter.view.*
import kotlinx.android.synthetic.main.layout_report_date_picker.view.*
import kotlinx.android.synthetic.main.layout_spinner.view.*
import kotlinx.android.synthetic.main.layout_switch.view.*
import kotlinx.android.synthetic.main.layout_time_picker.view.*
import kotlinx.android.synthetic.main.layout_time_picker.view.tv_title_start_time
import java.text.DecimalFormat
import java.util.*

class CheckListAdapter(
    private val activity: CheckListActivity,
    private val subProcessList: List<SubProcessWithCheckList>,
    private val process_id: Int,
    private val product_id: Int,
    private var sequence: Int,
    private val processViewModel: ProcessViewModel
) : RecyclerView.Adapter<CheckListAdapter.MyViewHolder>() {

    companion object {
        private const val VIEW_TYPE0 = 0
        private const val VIEW_TYPE1 = 1
        private const val VIEW_TYPE2 = 2
        private const val VIEW_TYPE3 = 3
        private const val VIEW_TYPE4 = 4
        private const val VIEW_TYPE5 = 5
        private const val VIEW_TYPE6 = 6
        private const val VIEW_TYPE7 = 7
        private const val VIEW_TYPE8 = 8
    }

    override fun getItemViewType(position: Int): Int {
        val checkList = subProcessList[sequence].checkLists[position]

        //As we don't create any view for End Time SubTitle, so we return empty view.
        if (checkList.field_type.equals("TIME", ignoreCase = true) && previousCheckListHasSameNameAndType(position))
            return VIEW_TYPE8

        if (checkList.getAvailableChoices().isNotEmpty())
            return VIEW_TYPE7

        return when (checkList.field_type) {
            "REPORT DATE" -> VIEW_TYPE0
            "DATE" -> VIEW_TYPE1
            "TIME" -> VIEW_TYPE2
            "DATETIME" -> VIEW_TYPE3
            "DECIMAL" -> VIEW_TYPE4
            "INTEGER" -> VIEW_TYPE4
            "BOOLEAN" -> VIEW_TYPE5
            "TEXT" -> VIEW_TYPE6
            else -> VIEW_TYPE8
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): MyViewHolder {
        val resourceId = when (viewType) {
            VIEW_TYPE0 -> R.layout.layout_report_date_picker
            VIEW_TYPE1 -> R.layout.layout_date_picker
            VIEW_TYPE2 -> R.layout.layout_time_picker
            VIEW_TYPE3 -> R.layout.layout_date_time_picker
            VIEW_TYPE4 -> R.layout.layout_quantity_selecter
            VIEW_TYPE5 -> R.layout.layout_switch
            VIEW_TYPE6 -> R.layout.layout_edittext
            VIEW_TYPE7 -> R.layout.layout_spinner
            else -> R.layout.empty_layout
        }

        return MyViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(resourceId, viewGroup, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        try {
            val checkList = subProcessList[sequence].checkLists[position]
            when {
                checkList.field_type.equals("BOOLEAN", true) -> {
                    holder.switch?.text = checkList.name
                    holder.switch?.setOnCheckedChangeListener { _, isChecked ->
                        checkList.answer = isChecked.toString()
                    }
                    checkList.answer = holder.switch?.isChecked.toString()
                }
                checkList.field_type.equals("REPORT DATE", ignoreCase = true) -> {
                    holder.tvTitle?.text = checkList.name
                    holder.tvReportDate?.text = getCurrentDate()
                    holder.etDate?.setText(holder.tvReportDate?.text.toString())
                    checkList.answer = convertDateToServerFormat(holder.etDate?.text.toString())
                    holder.etDate?.setOnClickListener {
                        val materialDatePicker = MaterialDatePicker.Builder.datePicker().build()
                        materialDatePicker.show(
                            activity.supportFragmentManager,
                            materialDatePicker.toString()
                        )
                        materialDatePicker.addOnPositiveButtonClickListener {
                            holder.etDate?.setText(materialDatePicker.headerText)
                            checkList.answer =
                                convertDateToServerFormat(materialDatePicker.headerText)
                        }
                    }
                }
                checkList.field_type.equals("DATE", ignoreCase = true) -> {
                    holder.tvTitle?.text = checkList.name
                    holder.etDate?.setText(getCurrentDate())
                    checkList.answer = convertDateToServerFormat(holder.etDate?.text.toString())
                    holder.etDate?.setOnClickListener {
                        val materialDatePicker = MaterialDatePicker.Builder.datePicker().build()
                        materialDatePicker.show(
                            activity.supportFragmentManager,
                            materialDatePicker.toString()
                        )
                        materialDatePicker.addOnPositiveButtonClickListener {
                            holder.etDate?.setText(materialDatePicker.headerText)
                            checkList.answer =
                                convertDateToServerFormat(materialDatePicker.headerText)
                        }
                    }
                }
                checkList.field_type.equals("TIME", ignoreCase = true) -> {
                    if (previousCheckListHasSameNameAndType(position))
                        return

                    // if true, render two different view in a row else only one view in a row
                    if (nextCheckListHasSameNameAndType(position)){
                        val nextCheckList = subProcessList[sequence].checkLists[position + 1]
                        holder.tvTitle?.text = checkList.name
                        holder.tvKeyStartTime?.text = if (checkList.subtitle.isNotEmpty()) checkList.subtitle else "Start Time"
                        holder.tvKeyEndTime?.text = if (nextCheckList.subtitle.isNotEmpty()) nextCheckList.subtitle else "End Time"
                        holder.etStartTime?.setText("")
                        holder.etEndTime?.setText("")
                        holder.etStartTime?.setOnClickListener {
                            val materialTimePicker = MaterialTimePicker.Builder()
                                .setTimeFormat(TimeFormat.CLOCK_24H)
                                .build()
                            materialTimePicker.inputMode
                            materialTimePicker.show(activity.supportFragmentManager, "fragment_tag")
                            materialTimePicker.addOnPositiveButtonClickListener {
                                val newHour: Int = materialTimePicker.hour
                                val newMinute = materialTimePicker.minute

                                val hour = if (newHour < 10) "0$newHour" else "$newHour"
                                val minute = if (newMinute < 10) "0$newMinute" else "$newMinute"

                                holder.etStartTime?.setText(
                                    activity.getString(
                                        R.string.time_format,
                                        hour,
                                        minute
                                    )
                                )
                                checkList.answer = "$hour:$minute"
                            }
                        }

                        holder.tvKeyEndTime?.visibility = View.VISIBLE
                        holder.etEndTime?.visibility = View.VISIBLE
                        holder.imgEndTime?.visibility = View.VISIBLE
                        holder.etEndTime?.setOnClickListener {
                            val materialTimePicker = MaterialTimePicker.Builder()
                                .setTimeFormat(TimeFormat.CLOCK_24H)
                                .build()
                            materialTimePicker.show(activity.supportFragmentManager, "fragment_tag")
                            materialTimePicker.addOnPositiveButtonClickListener {
                                val newHour: Int = materialTimePicker.hour
                                val newMinute = materialTimePicker.minute

                                val hour = if (newHour < 10) "0$newHour" else "$newHour"
                                val minute = if (newMinute < 10) "0$newMinute" else "$newMinute"

                                holder.etEndTime?.setText(
                                    activity.getString(
                                        R.string.time_format,
                                        hour,
                                        minute
                                    )
                                )
                                nextCheckList.answer = "$hour:$minute"
                            }
                        }
                    } else {
                        holder.tvTitle?.text = checkList.name
                        holder.tvKeyStartTime?.text = activity.getString(R.string.time)
                        holder.etStartTime?.setText("")
                        holder.etEndTime?.setText("")
                        holder.tvKeyEndTime?.visibility = View.INVISIBLE
                        holder.etEndTime?.visibility = View.INVISIBLE
                        holder.imgEndTime?.visibility = View.INVISIBLE
                        holder.etStartTime?.setOnClickListener {
                            val materialTimePicker = MaterialTimePicker.Builder()
                                .setTimeFormat(TimeFormat.CLOCK_24H)
                                .build()
                            materialTimePicker.show(activity.supportFragmentManager, "fragment_tag")
                            materialTimePicker.addOnPositiveButtonClickListener {
                                val newHour: Int = materialTimePicker.hour
                                val newMinute = materialTimePicker.minute

                                val hour = if (newHour < 10) "0$newHour" else "$newHour"
                                val minute = if (newMinute < 10) "0$newMinute" else "$newMinute"

                                holder.etStartTime?.setText(
                                    activity.getString(
                                        R.string.time_format,
                                        hour,
                                        minute
                                    )
                                )
                                checkList.answer = "$hour:$minute"
                            }
                        }
                    }
                }
                checkList.field_type.equals("DATETIME", ignoreCase = true) -> {
                    holder.tvTitle?.text = checkList.name
                    holder.etDate?.setText(getCurrentDate())
                    checkList.answer = convertDateToServerFormat(holder.etDate?.text.toString())
                    holder.etDate?.setOnClickListener {
                        val materialDatePicker = MaterialDatePicker.Builder.datePicker().build()
                        materialDatePicker.show(
                            activity.supportFragmentManager,
                            materialDatePicker.toString()
                        )
                        materialDatePicker.addOnPositiveButtonClickListener {
                            holder.etDate?.setText(materialDatePicker.headerText)
                            checkList.answer = convertDateToServerFormat(materialDatePicker.headerText)
                                .plus(" ").plus(holder.etTime?.text.toString())
                        }
                    }

                    holder.etTime?.setText("")
                    holder.etTime?.setOnClickListener {
                        val materialTimePicker = MaterialTimePicker.Builder()
                            .setTimeFormat(TimeFormat.CLOCK_24H)
                            .build()
                        materialTimePicker.show(activity.supportFragmentManager, "fragment_tag")
                        materialTimePicker.addOnPositiveButtonClickListener {
                            val newHour: Int = materialTimePicker.hour
                            val newMinute = materialTimePicker.minute

                            val hour = if (newHour < 10) "0$newHour" else "$newHour"
                            val minute = if (newMinute < 10) "0$newMinute" else "$newMinute"

                            holder.etTime?.setText(activity.getString(
                                    R.string.time_format,
                                    hour,
                                    minute
                                ))
                            checkList.answer = convertDateToServerFormat(holder.etDate?.text.toString())
                                .plus(" ").plus(holder.etTime?.text.toString())
                        }
                    }
                }

                checkList.getAvailableChoices().isNotEmpty() -> {
                    val unit = if (checkList.unit.isNullOrEmpty()) "" else " (${checkList.unit}) "
                    holder.tvKeyStartTime?.text =
                        if (checkList.unit.equals("Gram", ignoreCase = true) ||
                            checkList.unit.equals("Kilogram", ignoreCase = true) ||
                            checkList.unit.equals("Litre", ignoreCase = true))
                            "Select Weight($unit)" else "Select Answer"
                    holder.tvTitle?.text = checkList.name
                    val arrayAdapter = ArrayAdapter(
                        activity,
                        android.R.layout.simple_spinner_item,
                        checkList.getAvailableChoices()
                    )
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    holder.spinner?.adapter = arrayAdapter
                    holder.spinner!!.onItemSelectedListener = object : OnItemSelectedListener {
                        override fun onItemSelected(
                            parentView: AdapterView<*>?,
                            selectedItemView: View,
                            position: Int,
                            id: Long
                        ) {
                            checkList.answer =
                                if (!arrayAdapter.isEmpty) arrayAdapter.getItem(position)!! else ""
                        }

                        override fun onNothingSelected(parentView: AdapterView<*>?) {}
                    }

                    checkList.answer = if (!arrayAdapter.isEmpty) arrayAdapter.getItem(0)!! else ""
                }
                checkList.field_type.equals("TEXT", ignoreCase = true) -> {
                    holder.tvTitle?.text = checkList.name
                    holder.editText?.addTextChangedListener(object : TextWatcher{
                        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
                        override fun afterTextChanged(s: Editable?) {
                            checkList.answer = s.toString()
                        }
                    })
                }
                checkList.unit == null -> {
                    holder.tvTitle?.text = checkList.name
                    holder.tvQty?.text = if (checkList.getAvailableRange().isNotEmpty()) checkList.getAvailableRange()[0] else "0"
                    holder.tvQty?.setOnClickListener {
                        showInputDialog("Enter Number", holder.tvQty!!, "", checkList)
                    }
                    adjustQuantity(holder, checkList, "")
                    checkList.answer = holder.tvQty?.text.toString()
                }
                checkList.unit.equals("MINUTE", ignoreCase = true) || checkList.unit.equals("SECOND", ignoreCase = true) ->{
                    holder.tvTitle?.text = checkList.name
                    holder.tvQty?.text = if (checkList.getAvailableRange().isNotEmpty()) checkList.getAvailableRange()[0] else "0"
                    holder.tvQty?.setOnClickListener {
                        showInputDialog("Enter Duration (${checkList.unit!!.toLowerCase(Locale.getDefault())})", holder.tvQty!!, "", checkList)
                    }
                    adjustQuantity(holder, checkList, "")
                    checkList.answer = holder.tvQty?.text.toString()
                }
                checkList.unit.equals("Litre", ignoreCase = true) || checkList.unit.equals("Kilogram", ignoreCase = true) ||checkList.unit.equals("Gram", ignoreCase = true) -> {
                    holder.tvTitle?.text = checkList.name
                    holder.tvQty?.text = if (checkList.getAvailableRange().isNotEmpty()) checkList.getAvailableRange()[0] else "0"
                    holder.tvQty?.setOnClickListener {
                        showInputDialog("Enter Quantity (${checkList.unit})", holder.tvQty!!, "", checkList)
                    }
                    adjustQuantity(holder, checkList, "")
                    checkList.answer = holder.tvQty?.text.toString()
                }
                checkList.unit.equals("Celcius", ignoreCase = true) -> {
                    holder.tvTitle?.text = checkList.name
                    holder.tvQty?.text = if (checkList.getAvailableRange()
                            .isNotEmpty()
                    ) "${checkList.getAvailableRange()[0]}°" else "0°"
                    holder.tvQty?.setOnClickListener {
                        showInputDialog(
                            "Enter Temperature (°C)",
                            holder.tvQty!!,
                            "°",
                            checkList
                        )
                    }
                    adjustQuantity(holder, checkList, "°")
                    checkList.answer = holder.tvQty?.text.toString().replace("°", "")
                }
            }
        } catch (e: Exception) {
            activity.showToast("InvalidData: ${e.localizedMessage}")
        }
    }

    override fun getItemCount(): Int {
        return try {
            subProcessList[sequence].checkLists.size
        } catch (e: Exception) {
            e.printStackTrace()
            0
        }
    }


    private fun previousCheckListHasSameNameAndType(position: Int) : Boolean{
        return try {
            val checkList = subProcessList[sequence].checkLists[position]
            val previousCheckList = subProcessList[sequence].checkLists[position - 1]
            checkList.name.equals(previousCheckList.name, ignoreCase = true) &&
                    checkList.field_type.equals(previousCheckList.field_type, ignoreCase = true)
        } catch (e: Exception){
            false
        }
    }

    private fun nextCheckListHasSameNameAndType(position: Int) : Boolean{
        return try {
            val checkList = subProcessList[sequence].checkLists[position]
            val nextCheckList = subProcessList[sequence].checkLists[position + 1 ]
            checkList.name.equals(nextCheckList.name, ignoreCase = true) &&
                    checkList.field_type.equals(nextCheckList.field_type, ignoreCase = true)
        } catch (e: Exception){
            false
        }
    }

    internal fun saveAndClose() {
        showNextStage(false)
    }

    internal fun showPreviousStage() {
        /*if (sequence == 0) {
            activity.finish()
        }

        if (sequence > 0) {
            --sequence
            showProgress()
            notifyDataSetChanged()
        }*/
        activity.finish()
    }

    internal fun showNextStage(isSaveAndNext: Boolean = true) {
        try {
            //Field validation i.e. Check all fields have user input data
            subProcessList[sequence].checkLists.forEach {
                // As Date & Time picker and edit text would be empty always (i.e. No prefilled data)
                if (it.field_type.equals("REPORT DATE", ignoreCase = true) && (it.answer.isEmpty())) {
                    activity.showToast("Field cant be empty")
                    return
                }
                if (it.field_type.equals("TIME", ignoreCase = true) && it.answer.isEmpty()) {
                    activity.showToast("Field cant be empty")
                    return
                }
                if (it.field_type.equals("DATETIME", ignoreCase = true) && it.answer.length != 16) {
                    activity.showToast("Field cant be empty")
                    return
                }

                if (!it.field_type.equals("TEXT", ignoreCase = true) && it.answer.isEmpty()) {
                    activity.showToast("Field cant be empty")
                    return
                }
            }

            // Save current checkList answer
            processViewModel.updateCheckListAns(subProcessList[sequence].checkLists)

            // Set current subProcess status as Done, either it's first, mid or last subProcess.
            subProcessList[sequence].subProcess.status = "Done"
            processViewModel.updateSubProcess(subProcessList[sequence].subProcess)

            // if current subProcess is not last subProcess then, show next process
            if (sequence < subProcessList.size - 1) {
                if (!isSaveAndNext) {
                    activity.finish()
                    return
                }

                ++sequence
                activity.showCurrentProgress(subProcessList, sequence)
                notifyDataSetChanged()
                return
            }

            // if last subProcess, then set current Process is Done.
            if (sequence == subProcessList.size - 1) {
                activity.showCurrentProgress(subProcessList, sequence, updateEndTime = true)
                processViewModel.updateProcessDone(process_id)

                val intent = Intent(activity, ProcessCompleteActivity::class.java)
                intent.putExtra("process_id", process_id)
                intent.putExtra("product_id", product_id)
                activity.startActivity(intent)
                activity.finish()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            activity.showToast("SAVE AND NEXT: ${e.localizedMessage}")
        }
    }

    private fun adjustQuantity(holder: MyViewHolder, checkList: CheckList, unit: String) {
        val range: Array<String> = checkList.getAvailableRange()
        val minRange: Float = if (range.isNotEmpty()) range[0].toFloat() else 0.0f
        val maxRange: Float = if (range.isNotEmpty()) range[1].toFloat() else 0.0f
        val updateBy: Float =
            when {
                maxRange - minRange == 0f -> 0f
                maxRange - minRange <= 2.0 -> 0.1f
                else -> 1f
            }

        holder.btnMinus?.setOnClickListener {
            var qty = holder.tvQty?.text.toString().replace(unit, "").toFloat() - updateBy
            qty = DecimalFormat("#.##").format(qty).toFloat()
            if (qty >= minRange) {
                holder.tvQty?.text = qty.toString().plus(unit)
                checkList.answer = qty.toString()
            }
        }
        holder.btnPlus?.setOnClickListener {
            var qty = holder.tvQty?.text.toString().replace("°", "").toFloat() + updateBy
            qty = DecimalFormat("#.##").format(qty).toFloat()
            if (qty <= maxRange) {
                holder.tvQty?.text = qty.toString().plus(unit)
                checkList.answer = qty.toString()
            }
            if (maxRange == 0f) {
                val updateByOne = holder.tvQty?.text.toString().replace("°", "").toFloat() + 1
                holder.tvQty?.text = updateByOne.toString().plus(unit)
                checkList.answer = qty.toString()
            }
        }
    }

    private fun showInputDialog(title: String, tvQty: TextView, unit: String, checkList: CheckList) {
        val range: Array<String> = checkList.getAvailableRange()
        val minRange: Float = if (range.isNotEmpty()) range[0].toFloat() else 0.0f
        val maxRange: Float = if (range.isNotEmpty()) range[1].toFloat() else 0.0f

        val dialogView = LayoutInflater.from(activity).inflate(R.layout.edit_text, null)
        val imgClose = dialogView.img_close
        val tvError = dialogView.tv_error
        val tvTitle = dialogView.tv_title
        val etEntry = dialogView.et_entry
        val btnClear = dialogView.btn_clear
        val btnSubmit = dialogView.btn_submit

        tvTitle.text = title
        etEntry.setText(tvQty.text.toString().replace(unit, ""))

        val dialogBuilder = AlertDialog.Builder(activity, R.style.DialogTheme)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val alertDialog = dialogBuilder.create()
        imgClose.setOnClickListener { alertDialog.dismiss() }

        btnClear.setOnClickListener { etEntry.setText("") }
        btnSubmit.setOnClickListener {
            if (etEntry.text.toString().isNotEmpty()) {
                val enteredData = etEntry.text.toString().toFloat()
                if (minRange == 0f && maxRange == 0f || enteredData in minRange..maxRange) {
                    alertDialog.dismiss()
                    tvQty.text = enteredData.toString().plus(unit)
                    checkList.answer = enteredData.toString()
                } else {
                    val errorMsg = "Input should be between $minRange-$maxRange"
                    tvError.text = errorMsg
                    tvError.visibility = View.VISIBLE
                }
            } else {
                tvError.visibility = View.VISIBLE
            }
        }
        alertDialog.show()
    }


    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var switch: SwitchCompat? = view.switch1
        var tvTitle: TextView? = view.tv_title
        var tvReportDate: TextView? = view.tv_report_date
        var etDate: EditText? = view.et_date
        var etTime: EditText? = view.et_time

        var tvKeyStartTime: TextView? = view.tv_title_start_time
        var tvKeyEndTime: TextView? = view.tv_title_end_time
        var etStartTime: EditText? = view.et_start_time
        var etEndTime: EditText? = view.et_end_time
        var imgEndTime: ImageView? = view.img_end_time
        var tvQty: TextView? = view.tv_qty
        var btnMinus: ImageView? = view.btn_minus
        var btnPlus: ImageView? = view.btn_plus
        var spinner: Spinner? = view.spinner_answer
        var editText: EditText? = view.edittext
    }
}