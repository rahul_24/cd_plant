package com.example.cdpackaging.home

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cdpackaging.R
import com.example.cdpackaging.network.PlantApi
import com.example.cdpackaging.room.DataConverter
import com.example.cdpackaging.room.entity.SubProcess
import com.example.cdpackaging.room.view_model.ProcessViewModel
import com.example.cdpackaging.utils.*
import com.example.cdpackaging.utils.Utility.safeApi
import kotlinx.android.synthetic.main.layout_sub_process.*
import kotlinx.coroutines.flow.collect
import org.koin.android.viewmodel.ext.android.viewModel

class SubProcessActivity : AppCompatActivity() {

    private val processViewModel: ProcessViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_sub_process)
        img_back.setOnClickListener { finish() }

        val process_id = intent.getIntExtra("process_id", 0)
        val process_name = intent.getStringExtra("process_name") ?: ""
        val product_id = intent.getIntExtra("product_id", 0)
        tv_process.text = process_name

        processViewModel.getAllSubProcess(process_id).observe(this, {
            if (it.isNullOrEmpty())
                fetchSubProcess(process_id)
            else {
                val subProcessList = it.filter { it.active }
                if (subProcessList.isEmpty()){
                    tv_no_record.visibility = View.VISIBLE
                    view_card.visibility = View.GONE
                    recycler_sub_process.visibility = View.GONE
                } else{
                    tv_no_record.visibility = View.GONE
                    view_card.visibility = View.VISIBLE
                    recycler_sub_process.visibility = View.VISIBLE

                    //Show first 'Pending' subProcess in the list as 'In Progress' to make it enabled.As, other status list item are disabled by default.
                    subProcessList.find { it.status == "Pending" }?.status = "In Progress"
                    showSubProcess(subProcessList, process_id, process_name, product_id)
                    if(subProcessList[0].isSyncRequired)
                        fetchSubProcess(process_id, true)
                }
            }
        })
    }

    private fun fetchSubProcess(process_id: Int, showLoader : Boolean = true) {
        Log.d("ApiCall", "HomeActivity::fetchSubProcess")

        processViewModel.launchDataLoad {
            safeApi {
                PlantApi.RetroFitService.getSubProcess(Utility.getToken(), PlantApi.SUB_PROCESS.plus(process_id))
            }.collect { response ->
                when (response) {
                    is Result.Loading -> {
                        if (showLoader)
                            CustomProgressDialog.show(this)
                    }
                    is Result.Error -> {
                        if (showLoader){
                            CustomProgressDialog.dismiss()
                            showToast(response.code, response.detail)
                        }
                    }
                    is Result.NetworkError -> {
                        if (showLoader){
                            CustomProgressDialog.dismiss()
                            showToast()
                        }
                    }
                    is Result.Success -> {
                        CustomProgressDialog.dismiss()
                        processViewModel.addAllSubProcess(DataConverter().getSubProcessList(response.data, process_id))
                        processViewModel.addAllCheckList(DataConverter().getCheckList(process_id, response.data))
                    }
                }
            }
        }
    }

    private fun showSubProcess(subProcessList: List<SubProcess>, process_id: Int, process_name: String, product_id: Int) {
        recycler_sub_process.adapter = SubProcessAdapter(subProcessList, process_id, process_name, product_id)
        recycler_sub_process.layoutManager = LinearLayoutManager(this)
        if (recycler_sub_process.itemDecorationCount == 0)
            recycler_sub_process.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
    }
}