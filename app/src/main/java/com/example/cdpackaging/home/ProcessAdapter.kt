package com.example.cdpackaging.home

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.cdpackaging.PlantApplication
import com.example.cdpackaging.R
import com.example.cdpackaging.room.entity.Product
import com.example.cdpackaging.utils.loadImageWithCoil
import kotlinx.android.synthetic.main.cell_product.view.*


class ProcessAdapter(private val productList: List<Product>) :
    RecyclerView.Adapter<ProcessAdapter.ViewHolder>() {

    override fun getItemCount() = productList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cell_product, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val product = productList[position]
        holder.img_item.loadImageWithCoil(product.image_url)
        holder.tv_product.text = product.name
        holder.tv_process.text = product.process_name
        holder.tv_status.text = if (product.process_start_time != "" && product.process_end_time == "") "In Progress" else product.process_status

        /*if (holder.tv_status.text == "In Progress")
            holder.img_status.setBackgroundResource(R.drawable.bg_circular_yellow)
        else*/ if (product.process_status == "Done" && product.process_sync == false)
            holder.img_status.setBackgroundResource(R.drawable.bg_circular_red)
        else if (product.process_status == "Done" && product.process_sync == true)
            holder.img_status.setBackgroundResource(R.drawable.bg_circular_green)
        else if (product.process_status != "Done")
            holder.img_status.setBackgroundResource(0)

        holder.tv_status.setTextColor(holder.tv_status.context.resources.getColor(
            when (holder.tv_status.text) {
                "Done"-> { R.color.green }
                "In Progress" -> { R.color.yellow }
                "Pending" -> { R.color.red }
                else -> R.color.black
            }
        ))

        holder.root_process.setOnClickListener {
            val appSettings = PlantApplication.appInstance.getAppSettings()
            appSettings.productId = product.id
            appSettings.productName = product.name
            appSettings.processId = product.process_id
            appSettings.processName= product.process_name!!

            val intent = Intent(holder.root_process.context, SubProcessActivity::class.java)
            intent.putExtra("process_id", product.process_id)
            intent.putExtra("process_name", product.name)
            intent.putExtra("product_id", product.id)
            holder.root_process.context.startActivity(intent)
        }
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var root_process: ConstraintLayout = itemView.root
        var img_status: ImageView = itemView.img_status
        var img_item: ImageView = itemView.img_item
        var tv_product: TextView = itemView.tv_product
        var tv_process: TextView = itemView.tv_process
        var tv_status: TextView = itemView.tv_status
    }
}