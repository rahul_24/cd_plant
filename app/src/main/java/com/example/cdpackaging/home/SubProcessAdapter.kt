package com.example.cdpackaging.home

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.cdpackaging.R
import com.example.cdpackaging.room.entity.SubProcess
import kotlinx.android.synthetic.main.cell_sub_process.view.*


class SubProcessAdapter(
    private val subProcessList: List<SubProcess>,
    private val process_id: Int,
    private val process_name: String,
    private val product_id: Int
) :
    RecyclerView.Adapter<SubProcessAdapter.ViewHolder>() {

    override fun getItemCount() = subProcessList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.cell_sub_process, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val subProcess = subProcessList[position]

        //holder.tv_sequence.text = subProcess.sequence.toString()
        holder.tv_sequence.text = (position + 1).toString()
        holder.tv_sequence.setBackgroundResource(
            when (subProcess.status) {
                "In Progress" -> R.drawable.bg_rounded_yellow
                "Pending" -> R.drawable.bg_rounded_red
                else -> R.drawable.bg_rounded_green
            }
        )

        holder.tv_sub_process.text = subProcess.name
        holder.tv_status.text = subProcess.status
        holder.tv_status.setTextColor(
            holder.tv_status.context.resources.getColor(
                when (subProcess.status) {
                    "In Progress" -> R.color.yellow
                    "Pending" -> R.color.red
                    else -> R.color.green
                }
            )
        )

        holder.root.setOnClickListener {
            if (holder.tv_status.text == "In Progress") {
                val intent = Intent(holder.root.context, CheckListActivity::class.java)
                intent.putExtra("product_id", product_id)
                intent.putExtra("process_id", process_id)
                intent.putExtra("process_name", process_name)
                intent.putExtra("subprocess_index", position)
                holder.root.context.startActivity(intent)
            }
        }
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var root: ConstraintLayout = itemView.root
        var tv_sequence: TextView = itemView.tv_sequence
        var tv_sub_process: TextView = itemView.tv_sub_process
        var tv_status: TextView = itemView.tv_status
    }
}