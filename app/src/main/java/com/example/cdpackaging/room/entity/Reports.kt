package com.example.cdpackaging.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.squareup.moshi.JsonClass

@Entity
data class ReportsDate(
    @PrimaryKey
    var date: String,
    var count: Int = 0,
    var next: String? = null,
    var previous: String?= null,
)

@Entity(primaryKeys = ["date", "start_time", "process_id"])
@JsonClass(generateAdapter = true)
data class ReportsProduct(
    var date: String,
    var batchId: String? = null,
    var production_centre: String? = null,
    var product: String? = null,
    var process_id: Int,
    var process: String,
    var start_time: String,
    var end_time: String,
    var user_id: Int? = null,
)

@Entity(primaryKeys = ["date", "start_time", "process_id", "sub_process_id"])
@JsonClass(generateAdapter = true)
data class ReportsSubProcess(
    var date: String,
    var process_id: Int,
    var sub_process_id: Int,
    var sub_process: String? = null,
    var start_time: String,
    var end_time: String,
)

@Entity(primaryKeys = ["date", "start_time", "sub_process_id", "id"])
@JsonClass(generateAdapter = true)
data class ReportsChecklist(
    var date: String,
    var id: Int,
    var process: String,
    var sub_process_id: Int,
    var sub_process: String? = null,
    var name: String? = null,
    var subtitle: String? = null,
    var sequence: Int? = null,
    var description: String? = null,
    var field_type: String? = null,
    var choices: String,
    var range: String,
    var unit: String? = null,
    var answer: String,
    var start_time: String,
    var end_time: String,

    ) {
    fun getAvailableChoices(): Array<String> {
        if (choices == "" || choices == "[]") {
            return emptyArray()
        }
        val listType = object : TypeToken<Array<String>>() {}.type
        return Gson().fromJson(choices, listType)
    }
}