package com.example.cdpackaging.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.JsonClass

@Entity(primaryKeys = ["process_id", "id"])
@JsonClass(generateAdapter = true)
data class Product(
    var id: Int,
    var name: String,
    var description: String,
    var final_product: String,
    var image_url: String,
    var process_id: Int,
    var process_name: String?,
    var process_start_time: String?,
    var process_end_time: String?,
    var process_status: String?,
    var process_sync: Boolean?,
)