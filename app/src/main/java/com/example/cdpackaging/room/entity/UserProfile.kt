package com.example.cdpackaging.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class UserProfile(
    var username: String? = null,
    var name: String? = null,
    @PrimaryKey
    var mobile: String,
    var email: String? = null,
    var address: String? = null,
    var centre_id: Int? = null,
    var centre_name: String? = null,
    var centre_description: String? = null,
    var centre_plant: String? = null,
    var centre_mcc_id: Int? = null,
    var centre_franchise_id: Int? = null,
    var centre_warehouse_id: Int? = null
)