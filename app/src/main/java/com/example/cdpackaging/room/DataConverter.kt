package com.example.cdpackaging.room

import com.example.cdpackaging.model.ProcessResponse
import com.example.cdpackaging.model.ProfileResponse
import com.example.cdpackaging.model.ReportsResponse
import com.example.cdpackaging.model.SubProcessResponse
import com.example.cdpackaging.room.entity.*
import com.google.gson.Gson

class DataConverter {

    fun getProductList(processResponse: List<ProcessResponse>): List<Product> {
        val productList = mutableListOf<Product>()
        processResponse.forEach {
            with(it.product) {
                productList.add(
                    Product(
                        id,
                        name,
                        description,
                        final_product,
                        image_url,
                        it.id,
                        it.name,
                        "",
                        "",
                        "Pending",
                        false
                    )
                )
            }
        }

        return productList
    }

    fun getSubProcessList(response: List<SubProcessResponse>, process_id: Int): List<SubProcess> {
        val allSubProcessList = mutableListOf<SubProcess>()
        response.forEach {
            allSubProcessList.add(
                SubProcess(
                    it.id,
                    process_id,
                    it.name,
                    it.description,
                    it.sequence,
                    it.active,
                    "Pending",
                    false
                )
            )
        }
        return allSubProcessList
    }

    fun getCheckList(process_id: Int, response: List<SubProcessResponse>): List<CheckList> {
        val allCheckList = mutableListOf<CheckList>()

        response.filter { it.checklists.isNotEmpty() }.forEach { subProcess ->
            subProcess.checklists.forEach {
                allCheckList.add(
                    CheckList(
                        it.id,
                        process_id,
                        subProcess.id,
                        it.name,
                        it.subtitle,
                        it.sequence,
                        it.description,
                        it.field_type,
                        Gson().toJson(it.choices),
                        Gson().toJson(it.range),
                        it.unit,
                        "",
                        it.active,
                        it.published
                    )
                )
            }
        }
        //Adding a static list at 0 index to show Report date in each Process's first subprocess.
        allCheckList.add(
            0,
            CheckList(
                0,
                process_id,
                allCheckList[0].subprocess_id,
                "Report Date",
                "",
                0,
                "",
                "REPORT DATE",
                "",
                "",
                null,
                "",
                true,
                true
            )
        )
        return allCheckList
    }

    fun getUserProfile(data: ProfileResponse): UserProfile {
        return with(data) {
            UserProfile(
                username, name, mobile!!, email, address,
                production_centre?.id,
                production_centre?.name,
                production_centre?.description,
                //production_centre?.plant?.toString(),
                //production_centre?.mcc_id,
                //production_centre?.franchise_id,
                //production_centre?.warehouse_id
            )
        }
    }


    fun getReportsDate(data: ReportsResponse, selectedDate: String): List<ReportsDate> {
        val reportsDateList = mutableListOf<ReportsDate>()
        var reportsDate = selectedDate

        if (selectedDate.isEmpty() && !data.results.isNullOrEmpty())
            reportsDate = data.results!![0].date

        data.results?.forEach {
            if (reportsDate == it.date) {
                reportsDateList.clear()
                reportsDateList.add(ReportsDate(it.date, data.count ?: 0, data.next, data.previous))
            } else {
                reportsDateList.clear()
                reportsDateList.add(ReportsDate(reportsDate))
                return@forEach
            }
        }
        return reportsDateList
    }

    fun getReportsProduct(data: ReportsResponse): List<ReportsProduct> {
        val reportsProductList = mutableListOf<ReportsProduct>()
        data.results?.forEach {
            reportsProductList.add(
                with(it) {
                    ReportsProduct(
                        date,
                        batch_id,
                        production_centre,
                        product,
                        process_id,
                        process!!,
                        start_time ?: "",
                        end_time ?: "",
                        user_id
                    )
                }
            )
        }

        return reportsProductList
    }

    fun getReportsSubProcess(data: ReportsResponse): List<ReportsSubProcess> {
        val reportsSubProcessList = mutableListOf<ReportsSubProcess>()
        data.results?.forEach {
            it.logsheet_details?.forEach { logsheetDetail ->
                reportsSubProcessList.add(
                    with(logsheetDetail) {
                        ReportsSubProcess(
                            it.date,
                            it.process_id,
                            checklist!!.sub_process_id,
                            checklist!!.sub_process,
                            it.start_time ?: "",
                            it.end_time ?: ""
                        )
                    }
                )
            }
        }

        return reportsSubProcessList
    }

    fun getReportsCheckList(data: ReportsResponse): List<ReportsChecklist> {
        val reportsCheckList = mutableListOf<ReportsChecklist>()
        data.results?.forEach {
            it.logsheet_details?.forEach { logsheetDetail ->
                reportsCheckList.add(
                    with(logsheetDetail.checklist!!) {
                        ReportsChecklist(
                            it.date,
                            id,
                            it.process!!,
                            sub_process_id,
                            sub_process,
                            name,
                            subtitle,
                            sequence,
                            description,
                            field_type,
                            Gson().toJson(choices),
                            Gson().toJson(range),
                            unit,
                            logsheetDetail.answer ?: "",
                            it.start_time ?: "",
                            it.end_time ?: "",
                        )
                    }
                )
            }
        }

        return reportsCheckList
    }
}