package com.example.cdpackaging.room.relation

import androidx.room.Embedded
import androidx.room.Relation
import com.example.cdpackaging.room.entity.CheckList
import com.example.cdpackaging.room.entity.SubProcess

data class SubProcessWithCheckList(
    @Embedded val subProcess: SubProcess,
    @Relation(
        parentColumn = "id",
        entityColumn = "subprocess_id"
    )
    val checkLists: List<CheckList>
)