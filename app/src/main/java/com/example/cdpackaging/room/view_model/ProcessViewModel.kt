package com.example.cdpackaging.room.view_model

import android.app.Application
import androidx.lifecycle.viewModelScope
import com.example.cdpackaging.PlantApplication
import com.example.cdpackaging.room.AppDatabase
import com.example.cdpackaging.room.entity.*
import com.example.cdpackaging.room.repository.ProcessRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ProcessViewModel(application: Application) : CustomViewModel(application) {

    private val repository: ProcessRepository

    init {
        val productDao = AppDatabase.getDatabase(application, viewModelScope).getProcessDao()
        repository = ProcessRepository(productDao)
    }

    fun getUserProfileLiveData(mobile: String) = repository.getUserProfileLiveData(mobile)

    suspend fun getUserProfile() = repository.getUserProfile()

    fun getAllProcess() = repository.getAllProcess()

    fun getAllSubProcess(process_id: Int) = repository.getAllSubProcess(process_id)

    fun getAllCheckList(process_id: Int) = repository.getAllCheckList(process_id)


    suspend fun getAllNonSyncDoneProcess() = repository.getAllNonSyncDoneProcess()

    suspend fun getPendingProcessCount() = repository.getPendingProcessCount()

    suspend fun getInProgressProcessCount() = repository.getInProgressProcessCount()

    suspend fun getInProgressOrNonSyncProcessCount() = repository.getInProgressOrNonSyncProcessCount()

    suspend fun getProcessStartEndTime(process_id : Int) = repository.getProcessStartEndTime(process_id)

    suspend fun getProcessCheckList(process_id: Int) = repository.getProcessCheckList(process_id)


    fun getReportsDate() = repository.getReportsDate()
    fun getReportsProcess(reportsDate : String) = repository.getReportsProcess(reportsDate)
    fun getReportsSubProcess(process_id: Int, startTime: String) = repository.getReportsSubProcess(process_id, startTime)
    fun getReportsCheckList(process_id: Int, startTime: String) = repository.getReportsCheckList(process_id, startTime)
    suspend fun getNextReportsProcessUrl(reportsDate : String) = repository.getNextReportsProcessUrl(reportsDate)


    fun addUserProfile(userProfile: UserProfile) = viewModelScope.launch {
        repository.addUserProfile(userProfile)
    }

    fun addAllProcess(list: List<Product>) = viewModelScope.launch {
        repository.addAllProcess(list)
    }

    fun addAllSubProcess(list: List<SubProcess>) = viewModelScope.launch {
        repository.addAllSubProcess(list)
    }

    fun addAllCheckList(list: List<CheckList>) = viewModelScope.launch {
        repository.addAllCheckList(list)
    }

    fun addReportsDate(reportsDate: String) = viewModelScope.launch {
        repository.addReportsDate(reportsDate)
    }
    fun addReportsData(
        reportsDate: List<ReportsDate>,
        reportsData: List<ReportsProduct>,
        reportsSubProcess: List<ReportsSubProcess>,
        reportsCheckList: List<ReportsChecklist>
    ) = viewModelScope.launch {
        repository.addReportsData(reportsDate, reportsData, reportsSubProcess,reportsCheckList)
    }
    fun silentAddReportsData(
        reportsData: List<ReportsProduct>,
        reportsSubProcess: List<ReportsSubProcess>,
        reportsCheckList: List<ReportsChecklist>
    ) = viewModelScope.launch {
        repository.silentAddReportsData(reportsData, reportsSubProcess,reportsCheckList)
    }

    fun updateProcessStartTime(process_id: Int, processStartTime: String) = viewModelScope.launch {
        repository.updateProcessStartTime(process_id, processStartTime)
    }

    fun updateProcessEndTime(process_id: Int, processEndTime: String) = viewModelScope.launch {
        repository.updateProcessEndTime(process_id, processEndTime)
    }

    fun updateCheckListAns(checkList: List<CheckList>) = viewModelScope.launch {
        repository.updateCheckListAns(checkList)
    }

    fun updateSubProcess(subProcess: SubProcess) = viewModelScope.launch {
        repository.updateSubProcess(subProcess)
    }

    fun updateProcessDone(process_id: Int) = viewModelScope.launch {
        repository.updateProcessDone(process_id)
    }

    suspend fun updateProcessSyncDone(process_id: Int) = repository.updateProcessSyncDone(process_id)

    fun restartAllProcess(invalidateData : Boolean = true) = viewModelScope.launch {
        repository.restartAllProcess(invalidateData)
    }

    fun clearAllTables() {
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                AppDatabase.closeDatabase(getPendingProcessCount() == 0)
            }
        }
    }

    fun clearAppData() {
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                AppDatabase.getDatabase(PlantApplication.appInstance, viewModelScope).clearAllTables()
            }
        }
    }
}