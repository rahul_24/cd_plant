package com.example.cdpackaging.room.entity

import androidx.room.Entity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.squareup.moshi.JsonClass

@Entity(primaryKeys = ["id", "subprocess_id"])
@JsonClass(generateAdapter = true)
data class CheckList(
    var id: Int,
    var process_id: Int = 0,
    var subprocess_id: Int = 0,
    var name: String,
    var subtitle: String,
    var sequence: Int,
    var description: String,
    var field_type: String,
    var choices: String,
    var range: String,
    var unit: String? = null,
    var answer: String,
    var active: Boolean,
    var published: Boolean,
){
    fun getAvailableChoices() : Array<String> {
        if (choices == "" || choices == "[]"){
            return emptyArray()
        }
        val listType = object : TypeToken<Array<String>>() {}.type
        return Gson().fromJson(choices, listType)
    }

    fun getAvailableRange() : Array<String> {
        if (range == "" || range == "[]"){
            return emptyArray()
        }
        val listType = object : TypeToken<Array<String>>() {}.type
        return Gson().fromJson(range, listType)
    }
}