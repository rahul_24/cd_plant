package com.example.cdpackaging.room.relation

import androidx.room.Embedded
import androidx.room.Relation
import com.example.cdpackaging.room.entity.CheckList
import com.example.cdpackaging.room.entity.ReportsChecklist
import com.example.cdpackaging.room.entity.ReportsSubProcess
import com.example.cdpackaging.room.entity.SubProcess

data class ReportsSubProcessWithCheckList(
    @Embedded var subProcess: ReportsSubProcess,
    @Relation(
        parentColumn = "sub_process_id",
        entityColumn = "sub_process_id"
    )
    var checkLists: List<ReportsChecklist>
)