package com.example.cdpackaging.room.entity

import androidx.room.Entity
import com.squareup.moshi.JsonClass

@Entity(primaryKeys = ["process_id", "id"])
@JsonClass(generateAdapter = true)
class SubProcess(
    var id: Int,
    var process_id: Int,
    var name: String,
    var description: String,
    var sequence: Int,
    var active: Boolean,
    var status : String,
    var isSyncRequired : Boolean
)