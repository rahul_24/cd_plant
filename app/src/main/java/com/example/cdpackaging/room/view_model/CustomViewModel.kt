package com.example.cdpackaging.room.view_model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.cdpackaging.room.AppDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.net.UnknownHostException

open class CustomViewModel(application: Application) : AndroidViewModel(application) {

    /**
     * Request a snackbar to display a string.
     *
     * This variable is private because we don't want to expose [MutableLiveData].
     *
     * MutableLiveData allows anyone to set a value, and [CustomViewModel] is the only
     * class that should be setting values.
     */
    private val _snackbar = MutableLiveData<String?>()

    /**
     * Request a snackbar to display a string.
     */
    val snackbar: LiveData<String?>
        get() = _snackbar


    private val _spinner = MutableLiveData(false)

    /**
     * Show a loading spinner if true
     */
    val spinner: LiveData<Boolean>
        get() = _spinner


    fun launchDataLoad(block: suspend () -> Unit) {
        // Here, viewModelScope is extension method of 'ViewModel.java' class that returns CoroutineScope
        // while launch() method is  extension method of CoroutineScope.kt.
        viewModelScope.launch {
            try {
                _spinner.value = true
                //mActivity?.showProgressDialog("Loading...")
                block()

            } catch (error: Throwable) {
                when (error) {
                    is UnknownHostException -> _snackbar.value =
                        "Please check your internet connection and try again."
                    else -> _snackbar.value = error.message
                }
                //mActivity?.showErrorDialog(error.message!!)
                //mActivity?.showDismissDialog("Error", "Something went wrong, try again later")

            } finally {
                _spinner.value = false
                _snackbar.value = null
                //mActivity?.hideProgressDialog()
            }
        }
    }
}