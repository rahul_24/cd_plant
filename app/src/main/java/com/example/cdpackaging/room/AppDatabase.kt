package com.example.cdpackaging.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.cdpackaging.PlantApplication
import com.example.cdpackaging.room.dao.ProcessDao
import com.example.cdpackaging.room.entity.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(entities = [UserProfile::class, Product::class, SubProcess::class, CheckList::class,
    ReportsDate::class, ReportsProduct::class, ReportsSubProcess::class, ReportsChecklist::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getProcessDao(): ProcessDao

    companion object {
        @Volatile
        private var DB: AppDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope): AppDatabase {
            val databaseName = "Plant" + "_" + PlantApplication.appInstance.getAppSettings().contactNumber + ".db"

            return DB ?: synchronized(this) {
                DB = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    databaseName
                )
                    .addCallback(AppDatabaseCallback(scope))
                    .fallbackToDestructiveMigration()
                    .build()

                DB!!
            }
        }

        fun closeDatabase(clearAllTables : Boolean){
            if (clearAllTables) DB?.clearAllTables()
            DB?.close()
            DB = null
        }
    }

    private class AppDatabaseCallback(private val scope: CoroutineScope) : RoomDatabase.Callback() {
        override fun onOpen(db: SupportSQLiteDatabase) {
            super.onOpen(db)
            DB?.let {
                scope.launch {}
            }
        }
    }
}