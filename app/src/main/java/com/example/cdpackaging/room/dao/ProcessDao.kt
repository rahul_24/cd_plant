package com.example.cdpackaging.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.cdpackaging.room.entity.*
import com.example.cdpackaging.room.relation.ReportsSubProcessWithCheckList
import com.example.cdpackaging.room.relation.SubProcessWithCheckList

@Dao
interface ProcessDao {

    @Query("SELECT * FROM UserProfile where mobile = :mobile")
    fun getUserProfileLiveData(mobile: String): LiveData<UserProfile?>

    @Query("SELECT mobile, centre_id FROM UserProfile")
    suspend fun getUserProfile(): UserProfile?

    @Query("SELECT * FROM Product")
    fun getAllProcess(): LiveData<List<Product>?>

    @Query("SELECT * FROM SubProcess where process_id = :process_id order by sequence")
    fun getAllSubProcess(process_id: Int): LiveData<List<SubProcess>?>

    @Transaction
    @Query("SELECT * FROM SubProcess where process_id = :process_id and active = '1' order by sequence")
    fun getAllCheckList(process_id: Int): LiveData<List<SubProcessWithCheckList>?>

    @Query("SELECT id, name, description, final_product, image_url, process_id FROM Product where process_status = 'Done' and process_sync= '0'")
    suspend fun getAllNonSyncDoneProcess(): List<Product>

    @Query("select case when Total == Done then 0 else 1 end from (select count(*) as Total, (select count (*) from product where process_start_time <>''  and process_end_time <>''  and process_status = 'Done' ) as Done from Product)")
    suspend fun getPendingProcessCount(): Int


    @Query("SELECT count(*) FROM Product where process_start_time != '' and process_end_time =''")
    suspend fun getInProgressProcessCount(): Int

    @Query("SELECT count(*) FROM Product where process_start_time != '' and process_sync= '0'")
    suspend fun getInProgressOrNonSyncProcessCount(): Int


    @Query("SELECT * FROM Product where process_id =:process_id")
    suspend fun getProcessStartEndTime(process_id: Int): Product

    @Query("SELECT * FROM CheckList where process_id = :process_id and subprocess_id in (select id from subprocess where process_id = :process_id and active = '1') and active = '1' and published = '1'")
    suspend fun getProcessCheckList(process_id: Int): List<CheckList>

    /* Reports Data */
    @Query("SELECT distinct(date), '' as start_time, '' as end_time, '' as process_id, '' as process FROM ReportsProduct order by date desc")
    fun getReportsDate(): LiveData<List<ReportsProduct>?>

    @Query("SELECT * FROM ReportsProduct where date = :reportsDate order by start_time desc")
    fun getReportsProcess(reportsDate: String): LiveData<List<ReportsProduct>?>

    @Query("SELECT * FROM ReportsSubProcess where process_id = :process_id and start_time = :startTime")
    fun getReportsSubProcess(process_id: Int, startTime: String): LiveData<List<ReportsSubProcess>?>

    @Transaction
    @Query("SELECT * FROM ReportsSubProcess where process_id = :process_id and start_time= :startTime")
    fun getReportsCheckList(process_id: Int, startTime: String): LiveData<List<ReportsSubProcessWithCheckList>?>

    @Query("SELECT * FROM ReportsDate where date = :reportsDate")
    suspend fun getNextReportsProcessUrl(reportsDate: String): ReportsDate?



    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addUserProfile(userProfile: UserProfile)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addAllProcess(list: List<Product>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addAllSubProcess(list: List<SubProcess>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addAllCheckList(list: List<CheckList>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addReportsDate(reportsDate: ReportsDate)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addReportsData(
        reportsDate: List<ReportsDate>,
        reportsData: List<ReportsProduct>,
        reportsSubProcess: List<ReportsSubProcess>,
        reportsCheckList: List<ReportsChecklist>
    )

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun silentAddReportsData(
        reportsData: List<ReportsProduct>,
        reportsSubProcess: List<ReportsSubProcess>,
        reportsCheckList: List<ReportsChecklist>
    )

    @Query("Update Product set process_start_time = :endTime where process_id = :process_id")
    suspend fun updateProcessStartTime(process_id: Int, endTime: String)

    @Query("Update Product set process_end_time = :endTime where process_id = :process_id")
    suspend fun updateProcessEndTime(process_id: Int, endTime: String)

    @Update(entity = CheckList::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateCheckListAns(checkList: List<CheckList>)

    @Update(entity = SubProcess::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateSubProcess(subProcess: SubProcess)

    @Query("Update Product set process_status = 'Done' where process_id = :process_id")
    suspend fun updateProcessDone(process_id: Int)

    @Query("Update Product set process_sync = '1' where process_id = :process_id")
    suspend fun updateProcessSyncDone(process_id: Int) : Int


    @Query("delete from Product")
    suspend fun deleteProduct()

    @Query("update Product set process_start_time ='', process_end_time='', process_status = 'Pending', process_sync = '0'")
    suspend fun resetProduct()

    @Query("update SubProcess set status = 'Pending', isSyncRequired = :isSyncRequired")
    suspend fun resetSubProcess(isSyncRequired : Int)

    @Query("update CheckList set answer = ''")
    suspend fun resetCheckList()

    suspend fun restartAllProcess(invalidateData : Boolean) {
        if (invalidateData){
            deleteProduct()
            resetSubProcess(1)
            resetCheckList()
        } else {
            resetProduct()
            resetSubProcess(0)
            resetCheckList()
        }
    }
}