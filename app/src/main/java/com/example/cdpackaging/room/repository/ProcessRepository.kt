package com.example.cdpackaging.room.repository

import com.example.cdpackaging.room.dao.ProcessDao
import com.example.cdpackaging.room.entity.*

class ProcessRepository(private val processDao: ProcessDao) {

    fun getUserProfileLiveData(mobile: String) = processDao.getUserProfileLiveData(mobile)

    suspend fun getUserProfile() = processDao.getUserProfile()

    fun getAllProcess() = processDao.getAllProcess()

    fun getAllSubProcess(process_id : Int) = processDao.getAllSubProcess(process_id)

    fun getAllCheckList(process_id : Int) = processDao.getAllCheckList(process_id)


    suspend fun getAllNonSyncDoneProcess() = processDao.getAllNonSyncDoneProcess()

    suspend fun getPendingProcessCount() = processDao.getPendingProcessCount()

    suspend fun getInProgressProcessCount() = processDao.getInProgressProcessCount()

    suspend fun getInProgressOrNonSyncProcessCount() = processDao.getInProgressOrNonSyncProcessCount()

    suspend fun getProcessStartEndTime(process_id : Int) = processDao.getProcessStartEndTime(process_id)

    suspend fun getProcessCheckList(process_id: Int) = processDao.getProcessCheckList(process_id)


    fun getReportsDate() = processDao.getReportsDate()
    fun getReportsSubProcess(process_id: Int, startTime: String) = processDao.getReportsSubProcess(process_id, startTime)
    fun getReportsCheckList(process_id: Int, startTime: String) = processDao.getReportsCheckList(process_id, startTime)
    fun getReportsProcess(reportsDate : String) = processDao.getReportsProcess(reportsDate)
    suspend fun getNextReportsProcessUrl(reportsDate : String) = processDao.getNextReportsProcessUrl(reportsDate)


    suspend fun addUserProfile(userProfile: UserProfile) {
        processDao.addUserProfile(userProfile)
    }

    suspend fun addAllProcess(list: List<Product>) {
        processDao.addAllProcess(list)
    }

    suspend fun addAllSubProcess(list: List<SubProcess>) {
        processDao.addAllSubProcess(list)
    }

    suspend fun addAllCheckList(list: List<CheckList>) {
        processDao.addAllCheckList(list)
    }

    suspend fun updateProcessStartTime(process_id: Int, processStartTime: String){
        processDao.updateProcessStartTime(process_id, processStartTime)
    }

    suspend fun updateProcessEndTime(process_id: Int, processEndTime: String){
        processDao.updateProcessEndTime(process_id, processEndTime)
    }

    suspend fun updateCheckListAns(checkList: List<CheckList>) {
        processDao.updateCheckListAns(checkList)
    }

    suspend fun updateSubProcess(subProcess: SubProcess){
        processDao.updateSubProcess(subProcess)
    }
    suspend fun updateProcessDone(process_id: Int){
        processDao.updateProcessDone(process_id)
    }

    suspend fun updateProcessSyncDone(process_id: Int) = processDao.updateProcessSyncDone(process_id)


    suspend fun addReportsDate(selectedDate: String){
        processDao.addReportsDate(ReportsDate(selectedDate))
    }

    suspend fun addReportsData(
        reportsDate: List<ReportsDate>,
        reportsData: List<ReportsProduct>,
        reportsSubProcess: List<ReportsSubProcess>,
        reportsCheckList: List<ReportsChecklist>
    ) {
        processDao.addReportsData(reportsDate, reportsData, reportsSubProcess,reportsCheckList)
    }

    suspend fun silentAddReportsData(
        reportsData: List<ReportsProduct>,
        reportsSubProcess: List<ReportsSubProcess>,
        reportsCheckList: List<ReportsChecklist>
    ) {
        processDao.silentAddReportsData(reportsData, reportsSubProcess,reportsCheckList)
    }

    suspend fun restartAllProcess(invalidateData : Boolean){
        processDao.restartAllProcess(invalidateData)
    }
}