package com.example.cdpackaging.workManager

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.example.cdpackaging.HomeActivity
import com.example.cdpackaging.PlantApplication
import com.example.cdpackaging.R
import com.example.cdpackaging.model.LogSheetRequest
import com.example.cdpackaging.network.PlantApi
import com.example.cdpackaging.room.DataConverter
import com.example.cdpackaging.room.view_model.ProcessViewModel
import com.example.cdpackaging.utils.Result
import com.example.cdpackaging.utils.Utility
import com.example.cdpackaging.utils.Utility.safeApi
import com.example.cdpackaging.utils.showToast
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import org.koin.core.KoinComponent
import java.util.*
import kotlin.collections.HashMap

class DailyWorker(private val context: Context, params: WorkerParameters) :
    CoroutineWorker(context, params),
    KoinComponent {
    var result = "Sync done"
    var isManualSync = false

    private fun showNotification() {
        val intent = Intent(context, HomeActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent =
            PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        val channelId = context.getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(context, channelId).setColor(
            ContextCompat.getColor(context, R.color.colorPrimary)
        ).setSmallIcon(R.drawable.logo_country_delight).setContentTitle(
            context.getString(R.string.app_name)
        ).setContentText(context.getString(R.string.sync_in_progress)).setAutoCancel(
            true
        ).setPriority(NotificationCompat.PRIORITY_HIGH).setSound(defaultSoundUri).setContentIntent(
            pendingIntent
        )

        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Default Channel",
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0, notificationBuilder.build())
    }

    private fun hideNotification() {
        val notificationManager = NotificationManagerCompat.from(context)
        notificationManager.cancelAll()
    }

    override suspend fun doWork(): Result {
        try {
            Log.d(javaClass.simpleName, "Worker Started!")
            isManualSync = inputData.getBoolean("isManualSync", false)
            if (inputData.getBoolean("showNotification", false)) showNotification()

            val processViewModel = ProcessViewModel(PlantApplication.appInstance)
            val updateProcessList = processViewModel.getAllNonSyncDoneProcess()

            if (updateProcessList.isNullOrEmpty()) {
                showToast("Already synced")
                return Result.success()
            }

            // Sync Process data
            showToast("Sync started")
            updateProcessList.forEach { sendLogSheet(processViewModel, it.id, it.process_id) }

            showToast(result)
            delay(3000)
            hideNotification()

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return Result.success()
    }

    private suspend fun sendLogSheet(
        processViewModel: ProcessViewModel,
        product_id: Int,
        process_id: Int,
    ) {
        try {
            val userProfile = processViewModel.getUserProfile()
            val process = processViewModel.getProcessStartEndTime(process_id)
            val allCheckList = processViewModel.getProcessCheckList(process_id)

            var date = ""
            val detailsList = mutableListOf<HashMap<String, String>>()
            allCheckList.forEachIndexed { index, checkList ->
                if (index == 0 && checkList.id == 0) {
                    date = checkList.answer
                } else {
                    val hashMap = HashMap<String, String>()
                    hashMap["checklist_id"] = checkList.id.toString()
                    val field_type = if (checkList.field_type.equals("INTEGER", ignoreCase = true)) "decimal"
                    else checkList.field_type.toLowerCase(Locale.getDefault())
                    hashMap[field_type] = checkList.answer
                    detailsList.add(hashMap)
                }
            }

            if (userProfile?.centre_id == null && PlantApplication.appInstance.getAppSettings().prodCentreId == -1) {
                Log.d("Error", "DailyWorkerError::Invalid center_id")
                result = "Please restart app"
                return
            }

            val logSheetRequest =
                LogSheetRequest(
                    date,
                    userProfile?.centre_id ?: PlantApplication.appInstance.getAppSettings().prodCentreId,
                    product_id,
                    process_id,
                    process.process_start_time ?: Utility.getCurrentTime(),
                    process.process_end_time ?: Utility.getCurrentTime(),
                    detailsList
                )

            //Log.d("DailyWorker", Gson().toJson(logSheetRequest))
            Log.d("ApiCall", "DailyWorker::sendLogSheet")

            safeApi {
                PlantApi.RetroFitService.sendLogSheet(Utility.getToken(), logSheetRequest)
            }.collect { response ->
                if (response is com.example.cdpackaging.utils.Result.Error) {
                    showToast(response.code, response.detail)
                } else if (response is com.example.cdpackaging.utils.Result.Success) {
                    processViewModel.updateProcessSyncDone(process_id)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("DailyWorkerException", e.localizedMessage ?: "")
            showToast("Sync failure:  " + e.localizedMessage)
        }
    }

    private fun showToast(message: String) {
        if (isManualSync)
            Handler(Looper.getMainLooper()).postDelayed({ context.showToast(message) }, 100)
    }

    private fun showToast(code: Int?, detail: String?) {
        Handler(Looper.getMainLooper()).postDelayed({
            if (code == 401 || code == 406)
                PlantApplication.logout()
            if (detail.isNullOrEmpty())
                context.showToast("Something went wrong, try again later ($code).")
            else
                context.showToast(detail)
        }, 100)
    }
}